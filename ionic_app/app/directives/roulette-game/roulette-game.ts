import {Component, ElementRef, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';

// *******************  games  *******************
import { RouletteGame_LocalGame as LocalGame } from './roulette-game/games/local-game';
import { RouletteGame_SocketIoGame as SocketIoGame } from './roulette-game/games/socket-io-game';

// *******************  managers  *******************
import { RouletteGame_AssetsManager as AssetsManager } from './roulette-game/managers/assets-manager';
import { RouletteGame_ConfigManager as ConfigManager } from './roulette-game/managers/config-manager';
import { RouletteGame_LayersManager as LayersManager } from './roulette-game/managers/layers-manager';
import { RouletteGame_GameManager as GameManager } from './roulette-game/managers/game-manager';
import { RouletteGame_UiManager as UiManager } from './roulette-game/managers/ui-manager';

// *******************  entities  *******************
import { RouletteGame_Config as Config } from './roulette-game/entities/config';
import { RouletteGame_Signal as Signal } from './roulette-game/entities/signal';
import { RouletteGame_Page as Page } from './roulette-game/entities/page';
import { RouletteGame_Layer as Layer } from './roulette-game/entities/layer';
import { RouletteGame_TextRenderObject as TextRO } from './roulette-game/entities/render-object';

// *******************  interfaces  *******************
import { RouletteGame_iAssetsManager as iAssetsManager } from './roulette-game/interfaces/i-assets-manager';
import { RouletteGame_iConfigManager as iConfigManager } from './roulette-game/interfaces/i-config-manager';
import { RouletteGame_iLayersManager as iLayersManager } from './roulette-game/interfaces/i-layers-manager';
import { RouletteGame_iGameManager as iGameManager } from './roulette-game/interfaces/i-game-manager';
import { RouletteGame_iUiManager as iUiManager } from './roulette-game/interfaces/i-ui-manager';

import { RouletteGame_iResizeParams as iResizeParams } from './roulette-game/interfaces/i-resize-params';
import { RouletteGame_iConfigBetChip as iConfigBetChip } from './roulette-game/interfaces/i-config-bet-chip';
import { RouletteGame_iGame as iGame } from './roulette-game/interfaces/i-game';
import { RouletteGame_iSignal as iSignal } from './roulette-game/interfaces/i-signal';
import { RouletteGame_iPage as iPage } from './roulette-game/interfaces/i-page';
import { RouletteGame_iLayer as iLayer } from './roulette-game/interfaces/i-layer';

// *******************  enums  *******************
import { RouletteGame_GameType as GameType} from './roulette-game/enums/game-type';




@Component({
	templateUrl: 'build/directives/roulette-game/roulette-game.html',
	selector: 'roulette-game',
	providers: [AssetsManager, ConfigManager, LayersManager, UiManager, LocalGame, SocketIoGame, GameManager]
})
export class RouletteGame {

	// angular Directive DOM element
	private game_dom_element:any;

	// managers
	private assets:iAssetsManager;
	private config:iConfigManager;
	private layers:iLayersManager;
	private game:iGameManager;	// selected game manager
	private ui:iUiManager;

	// signals / events
	public onResize:iSignal<iResizeParams> = new Signal();

	// stage params
	private stageWidth:number = 10;
	private stageHeight:number = 10;

	// other stuff
	private _tmp:{ local_game:iGame, socket_io_game:iGame };
	private _loopActive:boolean = false;	// ali je trenutno loop / render aktiven
	private _loopFrame:number = 0;	// seštevek loop frame-ov za prikaz performančne statistike FPS
	private _is_init:boolean = false;
	private _should_loop:boolean = false;

	constructor(
		game_dom_element: ElementRef,
		assets:AssetsManager,
		config:ConfigManager,
		layers:LayersManager,
		ui:UiManager,
		game:GameManager,
		local_game:LocalGame,
		socket_io_game:SocketIoGame
		){
		
		this.game_dom_element = game_dom_element;
		this.assets = assets;
		this.config = config;
		this.layers = layers;
		this.ui = ui;
		this.game = game;

		// save both game managers for later selection
		this._tmp = {
			local_game: local_game,
			socket_io_game: socket_io_game,
		};

	}

	public initFromJson( file_path:string ){
		let a = this.assets;
		a.addJson( 'config_json', file_path );
		a.onLoaded.add( this.onConfigLoaded );
		a.loadAll();
	}

	public init( options:any ){
		// pripravimo configuration class
		this.config.init( options );

		// preverimo, če slučajno manjka kakšen zahtevan parameter
		if( ! this.config.paramsOk() ){
			this.config.getParamsOkErrors().forEach( ( erro_msg:string ) => {
				console.error( 'Config params check not ok:', erro_msg );
			} );
			throw new Error('Some required params missing from config.');
		}

		// *******************  v prenos in inicializacijo damo vse potrebno za igro  *******************
		this.addAssets();
		this.layers.setHolderElement( this.game_dom_element.nativeElement );

		// *******************  dodamo listenerje  *******************

		// ko se stage resize-a, to javimo v layer-je
		this.onResize.add( (params) => this.layers.resize(params) );
		// ko se naložijo vse datoteke, začnemo z aplikacijo, zato zdaj sprožimo lovdanje
		let a = this.assets;
		a.onLoaded.add( this.initRouletteGame );
		a.loadAll();
		// če se stage resize-a
		window.addEventListener('resize', this.windowResizeHandler );

		// *******************  sprožimo resize  *******************
		this.resize();
	}

	public start(){
		this._should_loop = true;
		if( ! this._is_init ){
			return;
		}
		this.startLoop();
	}

	public pause(){
		this._should_loop = false;
		this.stopLoop();
	}

	public resizeGame(){
		this.resize();
	}

	public destroy(){
		this._is_init = false;
		window.removeEventListener('resize', this.windowResizeHandler );

		// TODO - uredi vse stvari do konca
	}


	// ******************************************************************
	// *
	// *       PRIVATE METHODS
	// *
	// ******************************************************************
	
	/**
	 * Če smo igro inicializirali z RouletteGame::initFromJson(), se ta metoda sproži, ko smo nalovdali JSON
	 * s podatki, ki jih imamo iz JSONa, zaženemo "navaden" RouletteGame::init()
	 */
	private onConfigLoaded = ( hasErrors:boolean ) => {
		let a = this.assets;
		if( hasErrors ){
			// počistimo assets
			a.destroy();
			// prikažemo napako
			this.showError("Error loading json config file.");
			return;
		}

		// dobimo podatke iz JSON config datoteke
		let config_obj = a.get('config_json');

		// počistimo assets
		a.destroy();

		// sprožimo navadno inicializacijo
		this.init( config_obj );
	};

	/**
	 * Ko inicializiramo igro, dodamo datoteke v prenos in ko se prenos zaključi, lahko začnemo z igro
	 */
	private initRouletteGame = ( hasErrors:boolean ) => {
		if( hasErrors ){
			// TODO - kaj bomo naredili v tem primeru?
			this.assets.getSourcesWithErrors().forEach((src_with_error:string) => {
				console.log("ASSET MISSING:", src_with_error);
			});
			this.showError("Error loading requred files / assets for game.");
			return;
		}

		// pripravimo celoten ui
		this.ui.init();

		// dodamo listener za resize
		// this.onResize.add( (params:iResizeParams) => this.ui.resize(params) );
		// this.ui.resize( { stageWidth: this.stageWidth, stageHeight: this.stageHeight } );
		
		// *******************  zapišemo, da smo "ready"  *******************
		this._is_init = true;

		// *******************  sprožimo loop, če je bil zahtevan  *******************
		if( this._should_loop && ! this._loopActive ){
			this.start();
		}
	};

	/**
	 * nalovdamo vse slike in datoteke, potrebne za igro
	 */
	private addAssets(){
		let a = this.assets;

		a.dir = Config.assets_dir;

		// *******************  background  *******************
		a.addImage('bg_alpha', 'images/alpha_grid_bg.png' );

		// *******************  grids  *******************
		a.addImage('betting_field_square', Config.grid_square_src_img );
		a.addImage('betting_field_square_colors', Config.grid_square_src_colors );
		a.addJson('betting_field_square_keys', Config.grid_square_src_keys );

		a.addImage('betting_field_circle', Config.grid_circle_src_img );
		a.addImage('betting_field_circle_colors', Config.grid_circle_src_colors );
		a.addJson('betting_field_circle_keys', Config.grid_circle_src_keys );

		a.addImage('betting_field_portrait', Config.grid_portrait_src_img );
		a.addJson('betting_field_portrait_keys', Config.grid_portrait_src_keys );

		// *******************  betting chips  *******************
		Config.bet_chips.forEach(( bet_chip:iConfigBetChip, index:number) => {
			a.addImage('bet_chip_on_' + index, bet_chip.src_on );
			a.addImage('bet_chip_off_' + index, bet_chip.src_off );

			a.addImage('grid_chip_on_' + index, bet_chip.src_grid_on );
			a.addImage('grid_chip_off_' + index, bet_chip.src_grid_off );
		});

		// *******************  base  *******************
		a.addImage('base_bottom', Config.base_bottom_src );
		a.addImage('base_top', Config.base_top_src );
		a.addImage('base_bet_chips', "images/multipliers_63.png" );

		// *******************  wheel  *******************
		a.addImage('wheel_inner', Config.wheel_inner_src );
		a.addImage('wheel_outter', Config.wheel_outter_src );
		a.addImage('ball', Config.wheel_ball_src );
		a.addJson('numbers_rotations', Config.wheel_numbers_rotations_src );

		// *******************  buttons  *******************
		a.addImage('button', 'images/button_small.png' );
		a.addImage('button_kmet', 'images/button_small_kmet.png' );
		a.addImage('button_mini', 'images/button_mini.png' );
		a.addImage('switch_grid_to_square_button', 'images/button_change_table_view_1.png' );
		a.addImage('switch_grid_to_circle_button', 'images/button_change_table_view_0.png' );

		// *******************  icons  *******************
		a.addImage('icon_clear_all_bets', 'images/button_clear_bets_icon.png' );
		a.addImage('icon_clear_last_bet', 'images/button_clear_last_bets_icon.png' );
		a.addImage('icon_double_bet', 'images/button_double_bets_icon.png' );
		a.addImage('icon_repeat_bet', 'images/button_repeat_bets_icon.png' );
		a.addImage('icon_settings', 'images/settings_icon.png' );
	}

	/**
	 * starts the render loop
	 */
	private startLoop(){
		// si zapišemo, da smo aktivni v loop-u
		this._loopActive = true;
		this.loop();
		if( Config.stats_show_fps ){
			this.fps_init();
		}
	}

	/**
	 * ends the render loop
	 */
	private stopLoop(){
		this._loopActive = false;
	}

	/**
	 * posamezni loop, ki ga kličemo 60/s => 60FPS
	 */
	private loop(){
		if( ! this._loopActive){
			return;
		}

		// posodobimo parametre za pogled
		// this.ui.update();

		// izrišemo objekte z novimi parametri
		this.layers.render();



		// povečamo trenutni frame - potrebujemo za performančno statistiko
		if( Config.stats_show_fps ){
			this._loopFrame++;
		}

		// če želimo imeti custom FPS, to urejamo tu
		if( Config.use_fps > 0 ){
			setTimeout(() => {
		        window.requestAnimationFrame( () => this.loop() );
		        // Drawing code goes here
		    }, Math.floor( 1000 / Config.use_fps * 0.9 ) );
		}
		// če želimo imeti MAX FPS, ki ga podpira naprava in je ponavadi 60FPS
		else {
			window.requestAnimationFrame( () => this.loop() );
		}
	}

	/**
	 * če želimo prikazovati FPS performance  statistiko
	 */
	private fps_init(){
		let l = this.layers;

		let fps_ro:TextRO = new TextRO( 'FPS: ' );
		fps_ro.fontColor = "yellow";
		fps_ro.fontSize = 72;
		fps_ro.x = 50;
		fps_ro.y = 100;

		let stats_l:iLayer = l.getLayer('stats');
		stats_l.addRenderObject( fps_ro );

		let fps_dateStart:any = new Date;
		let fps_countFrames:any = this._loopFrame;

		setInterval( () => {
			let currDate:any = new Date;
			let timespan_miliseconds:any = currDate - fps_dateStart;
			let fps_frameTime = this._loopFrame / timespan_miliseconds * 1000;
			let fps_text:string = (fps_frameTime).toFixed(1);
			fps_dateStart = currDate;
			this._loopFrame = 0;
			fps_ro.text = 'FPS: ' + fps_text;
		}, 1000 );
	}

	/**
	 * parameter / funkcija, ki jo posredujemo v window.addEventListener('resize', windowResizeHandler );
	 * poskrbi za klic v RESIZE, ki sproži stage resize
	 * @type {[type]}
	 */
	private windowResizeHandler = () => {
		this.resize();
	};

	/**
	 * external call, ki nam javlja, da se je zgodil resize stage-a
	 */
	private resize(){
		this.stageWidth = this.game_dom_element.nativeElement.offsetWidth;
		this.stageHeight = this.game_dom_element.nativeElement.offsetHeight;


		this.onResize.trigger( { stageWidth: this.stageWidth, stageHeight: this.stageHeight  } );
	}






	private showError( msg:string ){
		// TODO - handle error - show user some notification
		console.error( msg );
	}
	
}   