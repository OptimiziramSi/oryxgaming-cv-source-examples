// *******************  interfaces  *******************
import { RouletteGame_iBettingChip as iBettingChip } from '../interfaces/i-betting-chip';
import { RouletteGame_iGridChipRenderObject as iGridChipRO } from '../interfaces/i-render-object';

export class RouletteGame_BettingChip implements iBettingChip {
	public value:number;
	public render_object:iGridChipRO;
}