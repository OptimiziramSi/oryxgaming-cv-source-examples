// *******************  interfaces  *******************
import { RouletteGame_iGridChip as iGridChip } from '../interfaces/i-grid-chip';
import { RouletteGame_iRenderObject as iRenderObject } from '../interfaces/i-render-object';
import { RouletteGame_iPayoutsByGridNumber as iPayoutsByGridNumber } from '../interfaces/i-payouts-by-grid-number';

// *******************  enums  *******************
import { RouletteGame_ChipState as ChipState } from '../enums/chip-state';


export class RouletteGame_GridChip implements iGridChip {
	public id:string;
	public grid_key_index:number;
	public betting_chip_index:number;
	public value:number;
	public state:ChipState = ChipState.WAITING_CONFIRMATION;
	public delete:boolean = false;
	public double_bet:boolean = false;
	public payouts:iPayoutsByGridNumber;
}