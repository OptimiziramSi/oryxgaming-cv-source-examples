// *******************  entities  *******************
import { RouletteGame_Config as Config } from './config';
import { RouletteGame_Utils as Utils } from './utils';

// *******************  interfaces  *******************
import { RouletteGame_iRenderObject as iRenderObject } from '../interfaces/i-render-object';
import { RouletteGame_iTextRenderObject as iTextRenderObject } from '../interfaces/i-render-object';
import { RouletteGame_iActiveImageRenderObject as iActiveImageRenderObject } from '../interfaces/i-render-object';
import { RouletteGame_iButtonRenderObject as iButtonRenderObject } from '../interfaces/i-render-object';
import { RouletteGame_iGridChipRenderObject as iGridChipRenderObject } from '../interfaces/i-render-object';
import { RouletteGame_iRectangleRenderObject as iRectangleRenderObject } from '../interfaces/i-render-object';


export class RouletteGame_RenderObject implements iRenderObject{
	
	protected _x:number = 0;
	protected _y:number = 0;
	protected _rotation:number = 0;
	protected _rotationOffsetX:number = 0;
	protected _rotationOffsetY:number = 0;
	protected _width:number = 0;
	protected _height:number = 0;
	protected _scale:number = 1;

	// support for sprite sheets
	protected _realX:number = 0;
	protected _realY:number = 0;
	protected _realWidth:number = 0;
	protected _realHeight:number = 0;

	// realni parametri za izris
	protected _ctxStageRatio:number = 1;
	protected _ctxTranslateX:number = 0;
	protected _ctxTranslateY:number = 0;
	protected _ctxRotationOffsetX:number = 0;
	protected _ctxRotationOffsetY:number = 0;
	protected _ctxX:number = 0;
	protected _ctxY:number = 0;
	protected _ctxWidth:number = 0;
	protected _ctxHeight:number = 0;
	protected _ctxChangedCallback:any = false;

	// param s katerim vemo, če moramo narediti nov render tega objekta
	protected _changed:boolean = false;

	// protected _layer:string = '';

	protected _visible:boolean = true;

	protected _onClick:any = false;

	protected _cacheUse:boolean = false;
	protected _cacheCanvas:any;
	protected _cacheCtx:any = false;

	constructor(){
		this._cacheUse = Config.render_use_cache;

		if( this._cacheUse ){
			this._cacheCanvas = document.createElement('canvas');
		}

		Config.onCtxParamsChange.add( this.onCtxParamsChange );
		this.onCtxParamsChange();
	}

	private onCtxParamsChange = () => {
		if(
			Config.ctxStageRatio != this._ctxStageRatio ||
			Config.ctxTranslateX != this._ctxTranslateX ||
			Config.ctxTranslateY != this._ctxTranslateY
			){

			this._ctxStageRatio = Config.ctxStageRatio;
			this._ctxTranslateX = Config.ctxTranslateX;
			this._ctxTranslateY = Config.ctxTranslateY;

			this.setCtxParams();
		}
	};

	public destroy(){
		Config.onCtxParamsChange.remove( this.onCtxParamsChange );
	}

	public get x():number{ return this._x; }
	public get y():number{ return this._y; }
	public set x(val:number){ if( val != this._x ){ this._x = val; this.setCtxParams(); } }
	public set y(val:number){ if( val != this._y){ this._y = val; this.setCtxParams(); } }

	public get rotation():number { return this._rotation; }
	public set rotation( val:number ) { if( Utils.precise_round( val, 0 ) != this._rotation ){ this._rotation = Utils.precise_round( val, 0 ); this._changed = true; } }
	public get rotationOffsetX():number { return this._rotationOffsetX; }
	public set rotationOffsetX( val:number ) { if( Utils.precise_round( val, 0 ) != this._rotationOffsetX ){ this._rotationOffsetX = Utils.precise_round( val, 0 ); this.setCtxParams(); } }
	public get rotationOffsetY():number { return this._rotationOffsetY; }
	public set rotationOffsetY( val:number ) { if( Utils.precise_round( val, 0 ) != this._rotationOffsetY ){ this._rotationOffsetY = Utils.precise_round( val, 0 ); this.setCtxParams(); } }

	public get centerX():number{ return this._x + this._width / 2; }
	public get centerY():number{ return this._y + this._height / 2; }
	public set centerX(val:number){ if( Utils.precise_round( val, 0) != this._x + this._width / 2 ){ this._x = Utils.precise_round( val, 0) - this._width / 2; this.setCtxParams(); } }
	public set centerY(val:number){ if( Utils.precise_round( val, 0) != this._y + this._height / 2){ this._y = Utils.precise_round( val, 0) - this._height / 2; this.setCtxParams(); } }

	public get visible():boolean { return this._visible; }
	public set visible(val:boolean) { if( val != this._visible){ this._changed = true } this._visible = val; }

	public get onClick():any { return this._onClick; }
	public set onClick(val:any) { this._onClick = val; }

	public get width():number{ return this._width; }
	public get height():number{ return this._height; }

	public get scale():number { return this._scale; }
	public set scale(val:number) { if( Utils.precise_round( val, 2) != this._scale ){ this._scale = Utils.precise_round( val, 2); this.setWidthAndHeight(); this.setCtxParams(); } }

	public get realX():number{ return this._realX; }
	public get realY():number{ return this._realY; }
	public get realWidth():number{ return this._realWidth; }
	public get realHeight():number{ return this._realHeight; }

	public get changed():boolean { return this._changed; }

	protected setWidthAndHeight(){
		this._width = Utils.precise_round( this._scale * this._realWidth, 0 );
		this._height = Utils.precise_round( this._scale * this._realHeight, 0 );
		this._cacheCtx = false;
		this._changed = true;
	}

	// iz podatkov X, Y, Width, Height, nam izračuna prave vrednosti, potrebne za izris na ctx (canvas context)
	private _setCtxParamsTimeout:any;
	protected setCtxParams( now:boolean = false ){
		clearTimeout( this._setCtxParamsTimeout );
		if( ! now ){
			this._setCtxParamsTimeout = setTimeout( () => this.setCtxParams(true), 5 );
			return;
		}

		this._ctxX = Utils.precise_round( this._ctxTranslateX + this._x * this._ctxStageRatio, 0 );
		this._ctxY = Utils.precise_round( this._ctxTranslateY + this._y * this._ctxStageRatio, 0 );
		this._ctxWidth = Utils.precise_round( this._width * this._ctxStageRatio, 0 );
		this._ctxHeight = Utils.precise_round( this._height * this._ctxStageRatio, 0 );

		this._ctxRotationOffsetX = Utils.precise_round( this._ctxX + this.rotationOffsetX * this._ctxStageRatio, 0 );
		this._ctxRotationOffsetY = Utils.precise_round( this._ctxY + this.rotationOffsetY * this._ctxStageRatio, 0 );

		if( this._ctxChangedCallback ){
			this._ctxChangedCallback();
		}

		this._changed = true;
	}

	public render( ctx ){
		
	}

}

export class RouletteGame_ImageRenderObject extends RouletteGame_RenderObject {

	private _img:any;
	constructor( img:any, cropX:number = 0, cropY:number = 0, cropW:number = 0, cropH:number = 0 ){
		super();

		this._img = img;
		this._realX = Utils.precise_round( cropX, 0 );
		this._realY = Utils.precise_round( cropY, 0 );
		this._realWidth = Utils.precise_round( cropW || this._img.width, 0 );	// if crop with is set / > 0, use it, otherwise use img width
		this._realHeight = Utils.precise_round( cropH || this._img.height, 0 );	// if crop height is set / > 0, use it, otherwise use img height

		this.setWidthAndHeight();
		this.setCtxParams();
	}

	public render( ctx ){
		if( this._visible ){
			let rotationTranslationX:number = 0;
			let rotationTranslationY:number = 0;

			if( this._rotation != 0 ){
				rotationTranslationX = this._ctxRotationOffsetX;
				rotationTranslationY = this._ctxRotationOffsetY;

				ctx.save();
				ctx.translate( rotationTranslationX, rotationTranslationY );
				ctx.rotate( this._rotation * Math.PI / 180 );
				ctx.translate( - rotationTranslationX, - rotationTranslationY );
			}

			if( this._cacheUse ){
				if( ! this._cacheCtx ){
					this._cacheCanvas.width = this._width;
					this._cacheCanvas.height = this._height;
					this._cacheCtx = this._cacheCanvas.getContext("2d");
					this._cacheCtx.drawImage( this._img, this._realX, this._realY, this._realWidth, this._realHeight, 0, 0, this._width, this._height );
				}
				ctx.drawImage( this._cacheCtx.canvas, this._x, this._y );
			} else {
				ctx.drawImage( this._img, this._realX, this._realY, this._realWidth, this._realHeight, this._ctxX, this._ctxY, this._ctxWidth, this._ctxHeight );
			}

			if( this._rotation != 0 ){
				ctx.restore();
				// ctx.translate( this._ctxX, this._ctxY );
				// ctx.rotate( - this._rotation * Math.PI / 180 );
				// ctx.translate( - this._ctxX, - this._ctxY );
			}
		}
		this._changed = false;
	}

}

export class RouletteGame_ActiveImageRenderObject extends RouletteGame_RenderObject implements iActiveImageRenderObject {

	private _img_off:any;
	private _img_on:any;
	private _active:boolean = false;

	constructor( img_off:any, img_on:any, cropX:number = 0, cropY:number = 0, cropW:number = 0, cropH:number = 0 ){
		super();

		this._img_off = img_off;
		this._img_on = img_on;
		this._realX = Utils.precise_round( cropX, 0 );
		this._realY = Utils.precise_round( cropY, 0 );
		this._realWidth = Utils.precise_round( cropW || this._img_off.width, 0 );	// if crop with is set / > 0, use it, otherwise use img width
		this._realHeight = Utils.precise_round( cropH || this._img_off.height, 0 );	// if crop height is set / > 0, use it, otherwise use img height

		this.setWidthAndHeight();
		this.setCtxParams();
	}

	public get active():boolean{ return this._active; }
	public set active(val:boolean){ if( val != this._active ) { this._active = val; this._changed = true; this._cacheCtx = false; } }

	public render( ctx ){
		if( this._visible ){
			if( this._cacheUse ){
				if( ! this._cacheCtx ){
					let img = this._active ? this._img_on : this._img_off;
					this._cacheCanvas.width = this._width;
					this._cacheCanvas.height = this._height;
					this._cacheCtx = this._cacheCanvas.getContext("2d");
					this._cacheCtx.drawImage( img, this._realX, this._realY, this._realWidth, this._realHeight, 0, 0, this._width, this._height );
				}
				ctx.drawImage( this._cacheCtx.canvas, this._x, this._y );
			} else {
				let img = this._active ? this._img_on : this._img_off;
				ctx.drawImage( img, this._realX, this._realY, this._realWidth, this._realHeight, this._ctxX, this._ctxY, this._ctxWidth, this._ctxHeight );
			}
		}
		this._changed = false;
	}

}

export class RouletteGame_GridChipRenderObject extends RouletteGame_ActiveImageRenderObject implements iGridChipRenderObject {

	private _value:number = 0;

	constructor( img_off:any, img_on:any, cropX:number = 0, cropY:number = 0, cropW:number = 0, cropH:number = 0 ){
		super( img_off, img_on, cropX, cropY, cropW, cropH );
	}

	public get value():number{ return this._value; }
	public set value(val:number){ if( val != this._value ) { this._value = val; this._changed = true; this._cacheCtx = false; } }

	public render( ctx ){
		if( this._visible ){
			if( false && this._cacheUse ){
				// TODO
			} else {
				// izrišemo sliko
				super.render( ctx );

				let fontSize:number = 30;
				let fontOffsetX:number = 0; //1 * Config.ctxStageRatio;
				let fontOffsetY:number = Utils.precise_round( 12 * Config.ctxStageRatio, 0 );
				// napišemo vrednost žetona
				ctx.font = Math.ceil( fontSize * Config.ctxStageRatio * this._scale ) + "px Arial";
				ctx.fillStyle = "black";
				ctx.textAlign = "center";
				ctx.fillText( this._value.toString(), this._ctxX + this._ctxWidth / 2 + fontOffsetX, this._ctxY + this._ctxHeight / 2 + fontOffsetY, this._width );
			}
		}
		this._changed = false;
	}

}

export class RouletteGame_TileRenderObject extends RouletteGame_RenderObject {

	private _img:any;
	constructor( img:any, cropX:number = 0, cropY:number = 0, cropW:number = 0, cropH:number = 0 ){
		super();

		this._img = img;
		this._realX = Utils.precise_round( cropX, 0 );
		this._realY = Utils.precise_round( cropY, 0 );
		this._realWidth = Utils.precise_round( cropW || this._img.width, 0 );	// if crop with is set / > 0, use it, otherwise use img width
		this._realHeight = Utils.precise_round( cropH || this._img.height, 0 );	// if crop height is set / > 0, use it, otherwise use img height

		this.setWidthAndHeight();
		this.setCtxParams();
	}

	public render( ctx ){
		if( this._visible ){
			var ptrn = ctx.createPattern(this._img, 'repeat'); // Create a pattern with this image, and set it to "repeat".
			ctx.fillStyle = ptrn;
			ctx.fillRect(0, 0, Config.ctxWidth, Config.ctxHeight);
		}
		this._changed = false;
	}

}

export class RouletteGame_TextRenderObject extends RouletteGame_RenderObject implements iTextRenderObject {

	private _text:string;
	private _fontSize:number = 30;
	private _fontFamily:string = "Arial";
	private _fontColor:string = "white";
	private _textAlign:string = 'left';

	private _ctxFontSize:number = 30;

	constructor( text:string ){
		super();

		this._text = text;

		this._ctxChangedCallback = this.setCtxFontSize;

		this.setCtxParams();
	}

	private setCtxFontSize = () => {
		this._ctxFontSize = Utils.precise_round( this._ctxStageRatio * this._fontSize, 0 );
	};

	public get text():string { return this._text; }
	public set text(val:string) { this._text = val; this._changed = true; }

	public get fontSize():number { return this._fontSize; }
	public set fontSize(val:number) { if( val != this._fontSize ) { this._fontSize = val; this._changed = true; } }

	public get fontFamily():string { return this._fontFamily; }
	public set fontFamily(val:string) { if( val != this._fontFamily ) { this._fontFamily = val; this._changed = true; } }

	public get fontColor():string { return this._fontColor; }
	public set fontColor(val:string) { if( val != this._fontColor ){ this._fontColor = val; this._changed = true; } }

	public get textAlign():string { return this._textAlign; }
	public set textAlign(val:string) { if( val != this._textAlign ){ this._textAlign = val; this._changed = true; } }

	// iz podatkov X, Y, Width, Height, nam izračuna prave vrednosti, potrebne za izris na ctx (canvas context)
	private _setCtxParamsTimeout2:any;
	protected setCtxParams( now:boolean = false ){
		clearTimeout( this._setCtxParamsTimeout2 );
		if( ! now ){
			this._setCtxParamsTimeout2 = setTimeout( () => this.setCtxParams(true), 5 );
			return;
		}

		// custom fix for Y position
		let y_offset:number = 25;	// TODO - najdi drugačen fix za tole tole zadevo. Verjetno je odvisno od font size ali česta takega

		this._ctxX = Utils.precise_round( this._ctxTranslateX + this._x * this._ctxStageRatio, 0 );
		this._ctxY = Utils.precise_round( this._ctxTranslateY + ( this._y + y_offset ) * this._ctxStageRatio, 0 );
		this._ctxWidth = Utils.precise_round( this._width * this._ctxStageRatio, 0 );
		this._ctxHeight = Utils.precise_round( this._height * this._ctxStageRatio, 0 );

		if( this._ctxChangedCallback ){
			this._ctxChangedCallback();
		}

		this._changed = true;
	}

	public render( ctx ){
		if( this._visible ){
			ctx.font = this._ctxFontSize + "px " + this._fontFamily;
			ctx.fillStyle = this._fontColor;
			ctx.textAlign = this._textAlign;

			let text_row_height:number = this._ctxFontSize * 1.1;
			let text_rows:string[] = this._text.split("\n");
			for (var i = 0; i < text_rows.length; ++i) {
				ctx.fillText( text_rows[i], this._ctxX, this._ctxY + text_row_height * i );
			}
		}
		this._changed = false;
	}

}

export class RouletteGame_ButtonRenderObject extends RouletteGame_RenderObject implements iButtonRenderObject {

	private _img:any;
	private _img_activated:any;
	private _icon:any;
	private _text:string;
	private _fontSize:number = 30;
	private _fontFamily:string = "Arial";
	private _fontColor:string = "black";
	private _textAlign:string = 'center';

	private _activated:boolean = false;

	private _ctxFontSize:number = 30;

	constructor( img:any, text:string, img_activated:any = null ){
		super();

		this.img = img;
		if( null != img_activated ){
			this.img_activated = img_activated;
		}
		this._text = text;

		this._ctxChangedCallback = this.setCtxFontSize;

		this.setWidthAndHeight();
		this.setCtxParams();
	}

	public set img( img:any ){
		this._img = img;

		this._realX = 0;
		this._realY = 0;
		this._realWidth = Utils.precise_round( this._img.width, 0 );
		this._realHeight = Utils.precise_round( this._img.height, 0 );
	}

	private set img_activated( img:any ){
		this._img_activated = img;
	}

	private setCtxFontSize = () => {
		this._ctxFontSize = Utils.precise_round( this._ctxStageRatio * this._fontSize, 0 );
	};

	public get text():string { return this._text; }
	public set text(val:string) { this._text = val; this._changed = true; }

	public get fontSize():number { return this._fontSize; }
	public set fontSize(val:number) { if( val != this._fontSize ) { this._fontSize = val; this._changed = true; } }

	public get fontFamily():string { return this._fontFamily; }
	public set fontFamily(val:string) { if( val != this._fontFamily ) { this._fontFamily = val; this._changed = true; } }

	public get fontColor():string { return this._fontColor; }
	public set fontColor(val:string) { if( val != this._fontColor ){ this._fontColor = val; this._changed = true; } }

	public get textAlign():string { return this._textAlign; }
	public set textAlign(val:string) { if( val != this._textAlign ){ this._textAlign = val; this._changed = true; } }

	public get activated():boolean { return this._activated; }
	public set activated(val:boolean) { if( val != this._activated ){ this._activated = val; this._changed = true; } }

	public set icon(val:any) { this._icon = val; this._changed = true; }

	public render( ctx ){
		if( this._visible ){
			// izrišemo podlago gumba
			ctx.drawImage( this._img, this._ctxX, this._ctxY, this._ctxWidth, this._ctxHeight );

			// izrišemo activated sliko
			if( this._activated && this._img_activated ){
				ctx.drawImage( this._img_activated, this._ctxX, this._ctxY, this._ctxWidth, this._ctxHeight );
			}

			let text_y_offset:number = this._ctxFontSize ;	// TODO - najdi drugačen fix za tole tole zadevo. Verjetno je odvisno od font size ali česta takega

			let button_padding:number = Utils.precise_round( 0.2 * this._ctxHeight, 0 );
			// let text_x:number = Utils.precise_round( this._ctxX + button_padding, 0 );
			// let text_y:number = Utils.precise_round( this._ctxY + button_padding + text_y_offset * Config.ctxStageRatio, 0 );
			let text_x:number = Utils.precise_round( this._ctxX + this._ctxWidth / 2, 0 );
			let text_y:number = Utils.precise_round( this._ctxY + this._ctxHeight / 2 + text_y_offset * Config.ctxStageRatio, 0 );

			// če imamo ikono, jo izrišemo
			if( this._icon ){
				// ikono poravnamo na levo, če imamo tudi text, drugače poravnamo na sredino
				let icon_height:number = Utils.precise_round( this._ctxHeight - 2*button_padding, 0 );
				let icon_width:number = Utils.precise_round( icon_height * ( this._icon.width / this._icon.height ), 0 );
				let icon_x:number = Utils.precise_round( "" != this._text ? this._ctxX + button_padding : this._ctxX + ( this._ctxWidth - icon_width ) / 2, 0 );
				let icon_y:number = Utils.precise_round( this._ctxY + button_padding, 0 );

				text_x = Utils.precise_round( icon_x + icon_width + ( this._ctxWidth - icon_width - button_padding*2 ) / 2, 0 );

				ctx.drawImage( this._icon, icon_x, icon_y, icon_width, icon_height );
			}

			// če imamo tekst, izpišemo še text
			if( "" != this._text ){
				ctx.font = "bold " + this._ctxFontSize + "px " + this._fontFamily;
				ctx.fillStyle = this._fontColor;
				ctx.textAlign = this._textAlign;

				// preračunati bomo morali pozicijo, ker glede na poravnavo, je tudi center teksta, ko ga postavljamo
				let toX:number = this._ctxX;
				let toY:number = this._ctxY;
				
				let text_row_height:number = this._ctxFontSize * 1.1;
				let text_rows:string[] = this._text.split("\n");
				text_y -= ( text_rows.length - 1 ) * ( text_row_height / 2 );
				for (var i = 0; i < text_rows.length; ++i) {
					ctx.fillText( text_rows[i], text_x, text_y + text_row_height * i );
				}
			}
		}
		this._changed = false;
	}

}

export class RouletteGame_RectangleRenderObject extends RouletteGame_RenderObject implements iRectangleRenderObject {

	private _color:string = "#ff0000";
	constructor( width:number = 100, height:number = 100 ){
		super();

		this._realWidth = Utils.precise_round( width, 0 );
		this._realHeight = Utils.precise_round( height, 0 );

		this.setWidthAndHeight();
		this.setCtxParams();
	}

	public get color():string { return this._color; }
	public set color( val:string ) { if( val != this._color ){ this._color = val; this._changed = true; } }

	public set width( val:number ){ if( val != this._width ){ this._width = val; this.setCtxParams(); } }
	public set height( val:number ){ if( val != this._height ){ this._height = val; this.setCtxParams(); } }

	public render( ctx ){
		if( this._visible ){
			if( this._cacheUse ){
				// TODO
			} else {
				ctx.fillStyle = this._color;
				ctx.fillRect( this._ctxX, this._ctxY, this._ctxX + this._ctxWidth, this._ctxY + this._ctxHeight );
			}
		}
		this._changed = false;
	}

}