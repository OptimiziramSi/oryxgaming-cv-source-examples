export interface RouletteGame_iAsset {
	type:string,	// tip datoteke
	src:string,	// src datoteke
	loaded:boolean,	// je že zlovdana
	error:boolean,	// ima napake / datoteke nismo mogli dobiti
	loading:boolean,	// se trenutno prenaša
	data:any	// vrednost / vsebina datoteke, lahko DOM IMG, lahko je OBJECT, če smo lovdali JSON, lahko je file content
}