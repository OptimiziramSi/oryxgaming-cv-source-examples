// *******************  interfaces  *******************
import { RouletteGame_iSignal as iSignal } from './i-signal';

export interface RouletteGame_iAssetsManager {

	/**
	 * Parameter, ki se uporabi kot predpona poti k source-u / poti datoteke, ko dodajamo datoteko
	 * @type {string}
	 */
	dir:string;

	/**
	 * Parameter / signal, ki se sproži, ko so prenese vse dodane datoteke. Ob klicu vrača boolean parameter, ki pove, če je pri prenosu prišlo do težav / napak
	 * @type {iSignal<boolean>}
	 */
	onLoaded:iSignal<boolean>


	/**
	 * Dodamo sliko v seznam datotek, ki jo bomo kasneje uporabljali v aplikaciji
	 * @param {string} asset_id  ime datoteke, preko katerega bomo datoteko kasneje v aplikaciji lahko pridobili
	 * @param {string} asset_src source / pot do datoteke za prenos
	 */
	addImage( asset_id:string, asset_src:string ) : void;

	/**
	 * Dodamo datoteko, katere vsebino bomo potrebovali kasneje v aplikaciji
	 * @param {string} asset_id  ime datoteke, preko katerega bomo vsebino datoteke kasneje v aplikaciji lahko pridobili
	 * @param {string} asset_src source / pot do datoteke za prenos
	 */
	addContent( asset_id:string, asset_src:string ) : void;

	/**
	 * Dodamo datoteko, ki ima vsebino zapisano v obliki JSONa in bomo kasneje v aplikaciji uporabljali JSON vsebino kot Object parametrov
	 * @param {string} asset_id  ime datoteke, preko katerega bomo dobili Objekt, ki je nastal iz JSON vsebine
	 * @param {string} asset_src source / pot do datoteke za prenos
	 */
	addJson( asset_id:string, asset_src:string ) : void;

	/**
	 * Sprožimo prenos vseh datotek, ki smo jih dodali v manager-ja.
	 */
	loadAll() : void;

	/**
	 * Vrača nam vsebino datoteke, ki smo jo poprej dodali v manager-ja
	 * @param  {string} asset_id ime datoteke, ki smo ga uporabili ob dodajanju te datoteke v prenos
	 * @return {any}             html img dom element | string | object => odvisno od tipa datoteke, ko smo datoteko dodajali
	 */
	get( asset_id:string ) : any;

	/**
	 * Vrača seznam poti datotek, ki so ob prenosu imele težave / se niso naložile
	 * @return {string[]} Seznam / array poti datotek, ki jih nismo mogli prenesti
	 */
	getSourcesWithErrors() : string[];

	/**
	 * počisti vse parametre za seboj, da lahko GarbageCollector naredi svoje
	 */
	destroy() : void;
}