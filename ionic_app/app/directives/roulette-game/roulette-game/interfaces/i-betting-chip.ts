// *******************  interfaces  *******************
import { RouletteGame_iGridChipRenderObject as iGridChipRO } from './i-render-object';

export interface RouletteGame_iBettingChip {
	value:number,
	render_object:iGridChipRO,
}