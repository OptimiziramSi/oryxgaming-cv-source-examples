export interface RouletteGame_iCircleRotations {
	/**
	 * grid_number: številka na bobnu
	 * ki drži vrednost kota v stopinjah, ki kaže ne želeno številko
	 * @type {[type]}
	 */
	[grid_number:number]:number;
}