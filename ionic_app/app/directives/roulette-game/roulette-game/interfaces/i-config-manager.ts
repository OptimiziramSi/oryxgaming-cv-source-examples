export interface RouletteGame_iConfigManager {
	/**
	 * inicializiramo konfiguracijo glede na podane parametre
	 * @param {Object} options parametri za aplikacijo
	 */
	init( options:Object ) : void;

	/**
	 * preveri, če imamo vse zahtevane parametre za aplikacijo in nam vrne odgovor, če je vse ok
	 * @return {boolean} true|false, glede na to, če je preverjanje bilo uspešno / je vse ok
	 */
	paramsOk() : boolean;

	/**
	 * dobimo seznam napak, ki jih je našla metoda paramsOk()
	 * @return {string[]} seznam / array napak
	 */
	getParamsOkErrors() : string[];

	/**
	 * počisti za seboj vse parametre, da jih kasneje lahko počisti GarbageCollector
	 */
	destroy() : void;
}