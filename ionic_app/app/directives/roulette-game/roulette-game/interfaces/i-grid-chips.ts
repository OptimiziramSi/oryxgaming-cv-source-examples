// *******************  interfaces  *******************
import { RouletteGame_iGridChip as iGridChip } from './i-grid-chip';

export interface RouletteGame_iGridChips {
	[grid_key_index:number] : iGridChip[];
}