// *******************  interfaces  *******************
import { RouletteGame_iLayer as iLayer } from './i-layer';
import { RouletteGame_iResizeParams as iResizeParams } from './i-resize-params';
import { RouletteGame_iPages as iPages } from './i-pages';
import { RouletteGame_iPage as iPage } from './i-page';

export interface RouletteGame_iLayersManager {
	/**
	 * določimo HTML DOM element, v katerega bomo dodajali canvase / layerje
	 * @param {any} dom_element
	 */
	setHolderElement( dom_element:any ) : void;

	/**
	 * dodamo layer entity v manager-ja, ki bo skrbel za resize, render itd
	 * @param {iLayer} layer
	 */
	addLayer( layer:iLayer ) : void;

	/**
	 * vrne iLayer entity, glede na zahtevan layer_id
	 * @param  {string} layer_id ime layerja, da ga v kodi lažje kličemo / prepoznamo
	 * @return {iLayer}
	 */
	getLayer( layer_id:string ) : iLayer;

	/**
	 * dodamo layer entity v manager-ja, ki bo skrbel za resize, render itd
	 * @param {iLayer} layer
	 */
	addPage( page:iPage ) : void;

	/**
	 * vrne iLayer entity, glede na zahtevan layer_id
	 * @param  {string} layer_id ime layerja, da ga v kodi lažje kličemo / prepoznamo
	 * @return {iLayer}
	 */
	getPage( page_id:string ) : iPage;

	/**
	 * seznam vseh registriranih page-ov
	 * @type {iPage[]}
	 */
	pages:iPages;

	/**
	 * Zunanja klic, ki nam posreduje informacijo o novih dimenzijah za izris. Metoda kasneje prilagodi layerje, canvas-e itd
	 * @param {iResizeParams} params podatki o novih dimenzijah za risanje / canvas-e
	 */
	resize( params:iResizeParams ) : void;

	/**
	 * Zunanji klic, ki nam sporoči, da sprožimo render na layer-jih, render objektih itd
	 */
	render() : void;

	/**
	 * počisti za sabo vse parametre, da lahko GarbageCollector naredi svoje
	 */
	destroy() : void;
}