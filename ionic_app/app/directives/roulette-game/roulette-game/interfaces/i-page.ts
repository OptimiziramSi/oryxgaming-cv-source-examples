// *******************  interfaces  *******************
import { RouletteGame_iAssetsManager as iAssetsManager } from '../interfaces/i-assets-manager';
import { RouletteGame_iLayersManager as iLayersManager } from '../interfaces/i-layers-manager';
import { RouletteGame_iGameManager as iGameManager } from '../interfaces/i-game-manager';
import { RouletteGame_iUiManager as iUiManager } from '../interfaces/i-ui-manager';

import { RouletteGame_iLayers as iLayers } from './i-layers';
import { RouletteGame_iLayer as iLayer } from './i-layer';
import { RouletteGame_iSignal as iSignal } from './i-signal';

export interface RouletteGame_iPage {

	/**
	 * da lahko page-u posredujemo assets manager-ja
	 * @type {iAssetsManager}
	 */
	am:iAssetsManager;

	/**
	 * da lahko page-u posredujemo layers manager-ja
	 * @type {iLayersmanager}
	 */
	lm:iLayersManager;

	/**
	 * da lahko posredujemo game manager
	 * @type {iGamemanager}
	 */
	gm:iGameManager;

	/**
	 * da lahko posredujemo ui manager-ja
	 * @type {iUiManager}
	 */
	um:iUiManager;

	/**
	 * id page-a, za lažji priklic / uporabo
	 * @type {string}
	 */
	id : string;

	/**
	 * koordinata X glede na stage size
	 * @type {number}
	 */
	x:number;

	/**
	 * koordinata Y glede na stage size
	 * @type {number}
	 */
	y:number;

	/**
	 * context calculated X position
	 * @type {number}
	 */
	ctxX:number;

	/**
	 * context calculated Y position
	 * @type {number}
	 */
	ctxY:number;

	/**
	 * če ta page prikazujemo / rendamo ali ne
	 * @type {boolean}
	 */
	visible:boolean;

	/**
	 * so bile spremembe / potrebuje nov render
	 * @type {boolean}
	 */
	changed:boolean;

	/**
	 * signal / event, ki se sproži, ko dodamo nov layer v page
	 * @type {iSignal<iLayer>}
	 */
	onAddLayer:iSignal<iLayer>;

	/**
	 * da lahko dobimo vse layerje, ki so del page-a
	 * @type {iLayers}
	 */
	layers:iLayers;

	/**
	 * inicializiramo page / postavimo elemente na svoje mesto itd
	 */
	init() : void;

	/**
	 * dodamo layer v trenutni page, s čimer si bomo pomagali za translacijo, pri animaciji page-a
	 * @param {iLayer} layer
	 */
	addLayer( layer:iLayer );

	/**
	 * počisti vse parametre za seboj
	 */
	destroy() : void;

}