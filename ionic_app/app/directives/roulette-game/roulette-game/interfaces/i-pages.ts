// *******************  interfaces  *******************
import { RouletteGame_iPage as iPage } from './i-page';

export interface RouletteGame_iPages {
	[page_id:string] : iPage	// seznam / object like array, kjer je ključ id layerja
}