export interface RouletteGame_iRenderObject {
	
	/**
	 * pozicija X pokoordinatnem sistemu, pri čemer vnašamo pozicijo glede na stage size in ne recnični size izrisa, zato bo element poskrbel sam
	 * @type {number}
	 */
	x : number;

	/**
	 * pozicija Y pokoordinatnem sistemu, pri čemer vnašamo pozicijo glede na stage size in ne recnični size izrisa, zato bo element poskrbel sam
	 * @type {number}
	 */
	y : number;

	/**
	 * Rotacija definirana v stopinjah
	 * @type {number}
	 */
	rotation : number;

	/**
	 * center rotacije X
	 * @type {number}
	 */
	rotationOffsetX : number;

	/**
	 * center rotacije Y
	 * @type {number}
	 */
	rotationOffsetY : number;

	/**
	 * podobno kot parameter X, samo da sam preračuna sredino objekta in primerno nastavi X. V resnici je to samo helper / lepši zapis
	 * @type {number}
	 */
	centerX : number;

	/**
	 * podobno kot parameter Y, samo da sam preračuna sredino objekta in primerno nastavi Y. V resnici je to samo helper / lepši zapis
	 * @type {number}
	 */
	centerY : number;

	/**
	 * ali naj bo objekt viden ali ne
	 * @type {boolean}
	 */
	visible : boolean;
		
	/**
	 * parametru lahko nastavimo funkcijo, ki naj se izvede ob kliku na objekt
	 * @type {any}
	 */
	onClick : any;

	/**
	 * širina objekta, glede na stage velikost in ne dejanski izris
	 * @type {number}
	 */
	width : number;

	/**
	 * višina objekta, glede na stage velikost in ne dejanski izris
	 * @type {number}
	 */
	height : number;

	/**
	 * scale elementa glede na stage velikost in ne dejanski izris
	 * @type {number}
	 */
	scale : number;

	/**
	 * dejanska velikost originalne slike / elementa
	 * @type {number}
	 */
	realWidth : number;

	/**
	 * dejanska velikost originalne slike / elementa
	 * @type {number}
	 */
	realHeight : number;

	/**
	 * ali so bili parametri za izris elementa spremenjeni / potrebujemo nov izris
	 * @type {boolean}
	 */
	changed : boolean;

	/**
	 * sproži render / izris objekta na context-u, ki ga je zunanji klic posredoval
	 * @param {any} ctx canvas context, na katerega želimo izrisati element
	 */
	render( ctx:any ) : void;

	/**
	 * počisti vse parametre, ki jih je ustvaril
	 */
	destroy() : void;
}

export interface RouletteGame_iTextRenderObject extends RouletteGame_iRenderObject {
	/**
	 * text content
	 * @type {string}
	 */
	text : string;

	/**
	 * velikost fonta, glede na stage size
	 * @type {number}
	 */
	fontSize : number;

	/**
	 * Družina / tip fonta
	 * @type {string}
	 */
	fontFamily : string;

	/**
	 * poravnava teksta
	 * @type {string}
	 */
	textAlign : string;

	/**
	 * barva teksta
	 * @type {string}
	 */
	fontColor : string;
}

export interface RouletteGame_iButtonRenderObject extends RouletteGame_iTextRenderObject {
	/**
	 * icon image / dom element
	 * @type {any}
	 */
	icon:any;

	/**
	 * ali je gumb "aktiviran" / "pritisnjen" / "izbran"
	 * @type {boolean}
	 */
	activated:boolean;
}

export interface RouletteGame_iActiveImageRenderObject extends RouletteGame_iRenderObject {
	/**
	 * povemo ali naj se izrisuje active slika ali not-active
	 * @type {boolean}
	 */
	active:boolean;
}

export interface RouletteGame_iGridChipRenderObject extends RouletteGame_iActiveImageRenderObject {
	/**
	 * povemo ali naj se izrisuje active slika ali not-active
	 * @type {boolean}
	 */
	value:number;
}

export interface RouletteGame_iRectangleRenderObject extends RouletteGame_iRenderObject {
	/**
	 * povemo ali naj se izrisuje active slika ali not-active
	 * @type {boolean}
	 */
	color:string;
}