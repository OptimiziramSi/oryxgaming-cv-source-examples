// *******************  interfaces  *******************
import { RouletteGame_iGameManager as iGameManager } from './i-game-manager';
import { RouletteGame_iResizeParams as iResizeParams } from './i-resize-params';
import { RouletteGame_iPage as iPage } from './i-page';

export interface RouletteGame_iUiManager {

	/**
	 * pripravimo vse potrebne objekte, ki jih bomo prikazovali
	 */
	init();

	/**
	 * ko želimo preklopiti na določen page
	 * @param {iPage} page
	 */
	switchPage( page:iPage ) : void;

	/**
	 * počisti vse za sabo
	 */
	destroy();
}