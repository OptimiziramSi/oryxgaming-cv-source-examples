import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

// *******************  entities  *******************
import { RouletteGame_Signal as Signal } from '../entities/signal';

// *******************  interfaces  *******************
import { RouletteGame_iAssetsManager as iAssetsManager } from '../interfaces/i-assets-manager';
import { RouletteGame_iSignal as iSignal } from '../interfaces/i-signal';
import { RouletteGame_iAssets as iAssets } from '../interfaces/i-assets';
import { RouletteGame_iAsset as iAsset } from '../interfaces/i-asset';

@Injectable()
export class RouletteGame_AssetsManager implements iAssetsManager {
	
	private http:Http;

	public dir:string = "";

	public onLoaded:iSignal<boolean> = new Signal();

	constructor( http:Http ){
		this.http = http;
	}

	private assets: iAssets = {};

	public addImage( asset_id:string, asset_src:string ){
		this.addAsset( asset_id, asset_src, 'image' );
	}

	public addContent( asset_id:string, asset_src:string ){
		this.addAsset( asset_id, asset_src, 'content' );
	}

	public addJson( asset_id:string, asset_src:string ){
		this.addAsset( asset_id, asset_src, 'json' );
	}

	public loadAll(){
		for (let asset_id in this.assets ) {
			let asset_obj = this.assets[ asset_id ];

			if( asset_obj.loaded || asset_obj.loading || asset_obj.error ){
				continue;
			}

			switch ( asset_obj.type ) {
				case "image":
					asset_obj.loading = true;

					let img = new Image();
					img.onload = ((event) => {
						asset_obj.loading = false;
						asset_obj.loaded = true;
						asset_obj.data = event.target;
						this.checkAllLoaded();
					});
					img.onerror = (() => {
						asset_obj.loading = false;
						asset_obj.error = true;
						this.checkAllLoaded();
					});
					img.src = asset_obj.src;

					break;

				case "content":
					asset_obj.loading = true;

					this.http
						.get(asset_obj.src)
						// .map( res => res.text() )
						.subscribe(
							data => {
								asset_obj.loading = false;
								asset_obj.loaded = true;
								asset_obj.data = data.text();
								this.checkAllLoaded();
							},
							error => {
								asset_obj.loading = false;
								asset_obj.error = true;
								this.checkAllLoaded();
							}
						);

					break;

				case "json":
					asset_obj.loading = true;

					this.http
						.get(asset_obj.src)
						// .map( res => res.json() )
						.subscribe(
							data => {
								asset_obj.loading = false;
								asset_obj.loaded = true;
								asset_obj.data = data.json();
								this.checkAllLoaded();
							},
							error => {
								asset_obj.loading = false;
								asset_obj.error = true;
								this.checkAllLoaded();
							}
						);

					break;
				
				default:

					throw new Error('Asset type "'+asset_obj.type+'" not recognized for asset with name "'+asset_id+'".');
			}
		}
	}

	public get( asset_id:string ):any {
		let find_asset_id = asset_id;
		for (let asset_id in this.assets ) {
			if( find_asset_id == asset_id ){
				let asset_obj = this.assets[ asset_id ];
				return asset_obj.data;
			}
		}
		throw new Error('Asset with name "'+asset_id+'" does not exist.');
	}

	public getSourcesWithErrors():string[]{
		let sources:string[] = [];
		for (let asset_id in this.assets ) {
			let asset_obj = this.assets[ asset_id ];

			if( asset_obj.error ){
				sources.push( "id: " + asset_id + " | src: " + asset_obj.src );
			}
		}
		return sources;
	}
	
	public destroy(){
		// TODO - check multiple times, that you clean everything
		this.assets = {};
		this.onLoaded.clear();
		
	}




	private addAsset( asset_id:string, asset_src:string, type:string = "image" ){
		if( this.assets.hasOwnProperty( asset_id ) ){
			throw new Error('Asset with name "'+asset_id+'" already exists.');
		}
		this.assets[ asset_id ] = {
			type: type,
			src: this.dir + asset_src,
			loaded: false,
			error: false,
			loading: false,
			data: null,
		};
	}

	private checkAllLoaded(){
		let hasErrors = false;
		for (let asset_id in this.assets ) {
			let asset_obj = this.assets[ asset_id ];

			if( asset_obj.loading ){
				// nekateri asseti se še nalagajo
				return;
			}

			if( asset_obj.error ){
				hasErrors = true;
			}
		}

		this.onLoaded.trigger( hasErrors );
	}
}