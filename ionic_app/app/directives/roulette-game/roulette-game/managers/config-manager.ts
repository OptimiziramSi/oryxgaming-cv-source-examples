import { Injectable } from '@angular/core';

// *******************  entities  *******************
import { RouletteGame_Signal as Signal } from '../entities/signal';
import { RouletteGame_Config as Config } from '../entities/config';

// *******************  interfaces  *******************
import { RouletteGame_iConfigManager as iConfigManager } from '../interfaces/i-config-manager';
import { RouletteGame_iConfigBetChip as iConfigBetChip } from '../interfaces/i-config-bet-chip';

// *******************  enums  *******************
import { RouletteGame_GameType as GameType } from '../enums/game-type';

@Injectable()
export class RouletteGame_ConfigManager implements iConfigManager {

	private _paramsOkErrors:string[] = [];

	contructor(){

	}

	public init( options:Object ){
		this.parseObject( options );
	}

	public paramsOk():boolean{
		let ok:boolean = true;
		let c = Config;
		
		
		return ok;
	}

	public getParamsOkErrors():string[] {
		// vračamo kopijo array-a
		return this._paramsOkErrors.slice();
	}

	public destroy(){
		Config.onCtxParamsChange.clear();
	}

	// json smo dobili kot object, nato sestavljamo parametre in jih nastavljamo v Config class
	// če imamo v JSONu zapis:
	// "game" : {
	// 		"type": "local",
	// },
	// "render" : {
	// 		"use_cache": true
	// }
	// 
	// bomo to prenesli v parametre v Config v:
	// public static game_type = local
	// public static render_use_cache = true
	private parseObject( object:Object, param:string = "" ){
		for( let key in object ){
			let param_key:string = ( "" != param ? param + "_" : "" ) + key;
			let value:any = object[ key ];

			if( Config.hasOwnProperty( param_key ) ){
				this.setParam( param_key, value);
				continue;
			}
			
			if( typeof value === 'object' && ! Array.isArray( value ) ){
				this.parseObject( value, param_key );
				continue;
			}
		}
	}

	/**
	 * ker parameter iz JSONa obstaja v Config, ga poskušamo nastavit
	 * vse pa je odvisno od tega, če se tip podatka "ujema" glede na zahteve v Config
	 */
	private setParam( param_key:string, value:any ){
		if( 'game_type' == param_key ){
			if( typeof value == 'string' ){
				if( 'local' == value ){ value = GameType.LOCAL; }
				if( 'socket_io' == value ){ value = GameType.LOCAL; }
			}
		}

		if( typeof value === typeof Config[ param_key ] && Array.isArray( value ) == Array.isArray( Config[ param_key ] ) ){
			Config[ param_key ] = value;
		}
	}
}