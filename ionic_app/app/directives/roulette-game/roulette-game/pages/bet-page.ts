// *******************  entities  *******************
import { RouletteGame_Page as Page } from '../entities/page';
import { RouletteGame_Config as Config } from '../entities/config';
import { RouletteGame_Layer as Layer } from '../entities/layer';
import { RouletteGame_GridChip as GridChip } from '../entities/grid-chip';
import { RouletteGame_BettingChip as BettingChip } from '../entities/betting-chip';
import {
	RouletteGame_RenderObject as RenderObject,
	RouletteGame_ImageRenderObject as ImageRO,
	RouletteGame_ActiveImageRenderObject as ActiveImageRO,
	RouletteGame_TextRenderObject as TextRO,
	RouletteGame_TileRenderObject as TileRO,
	RouletteGame_ButtonRenderObject as ButtonRO,
	RouletteGame_GridChipRenderObject as GridChipRO,
	} from '../entities/render-object';

// *******************  interfaces  *******************
import { RouletteGame_iAssetsManager as iAssetsManager } from '../interfaces/i-assets-manager';
import { RouletteGame_iLayersManager as iLayersManager } from '../interfaces/i-layers-manager';
import { RouletteGame_iGameManager as iGameManager } from '../interfaces/i-game-manager';
import { RouletteGame_iUiManager as iUiManager } from '../interfaces/i-ui-manager';

import { RouletteGame_iGridChips as iGridChips } from '../interfaces/i-grid-chips';
import { RouletteGame_iGridChip as iGridChip } from '../interfaces/i-grid-chip';
import { RouletteGame_iBettingChip as iBettingChip } from '../interfaces/i-betting-chip';
import { RouletteGame_iGridKeys as iGridKeys } from '../interfaces/i-grid-keys';
import { RouletteGame_iGridKey as iGridKey } from '../interfaces/i-grid-key';
import { RouletteGame_iConfigBetChip as iConfigBetChip } from '../interfaces/i-config-bet-chip';
import { RouletteGame_iMousePosition as iMousePosition } from '../interfaces/i-mouse-position';
import { RouletteGame_iRenderObject as iRenderObject } from '../interfaces/i-render-object';
import { RouletteGame_iTextRenderObject as iTextRO } from '../interfaces/i-render-object';
import { RouletteGame_iButtonRenderObject as iButtonRO } from '../interfaces/i-render-object';
import { RouletteGame_iActiveImageRenderObject as iActiveImageRO } from '../interfaces/i-render-object';
import { RouletteGame_iLayer as iLayer } from '../interfaces/i-layer';
import { RouletteGame_iGridChipRenderObject as iGridChipRO } from '../interfaces/i-render-object';

// *******************  enums  *******************
import { RouletteGame_GameState as GameState } from '../enums/game-state';
import { RouletteGame_ChipState as ChipState } from '../enums/chip-state';
import { RouletteGame_GridType as GridType } from '../enums/grid-type';
import { RouletteGame_GameConnectionStatus as GameConnectionStatus } from '../enums/game-connection-status';

export class RouletteGame_BetPage extends Page {

	// *******************  layers  *******************
	private grid_l:iLayer;
	private grid_chips_l:iLayer;
	private betting_chips_l:iLayer;
	private buttons_l:iLayer;
	private texts_l:iLayer;

	private _grid_type_selected:GridType;

	private _grid_square_keys:iGridKeys;
	private _grid_square_colors_ctx:any;	// canvas context
	private _grid_circle_keys:iGridKeys;
	private _grid_circle_colors_ctx:any;	// canvas context

	private _grid_chips:iGridChip[] = [];
	private _grid_chips_by_grid_key:iGridChips = {};

	private active_betting_chip_index:number = 0;
	// private select_betting_chip_opened:boolean = false;

	// *******************  render objects  *******************
	// bases
	private _base_top:iRenderObject;
	private _base_bottom:iRenderObject;
	// grids
	private _grid_square:iRenderObject;
	private _grid_circle:iRenderObject;
	// chips
	private _betting_chips:iBettingChip[] = [];
	private _betting_chips_bg:iRenderObject;
	private _grid_chips_render_objects:{ [grid_kex_index:number]:iGridChipRO } = {};
	// texts
	private _total_stake_lbl_txt:iTextRO;
	private _total_stake_txt:iTextRO;
	private _game_status_txt:iTextRO;
	private _credits_lbl_txt:iTextRO;
	private _credits_txt:iTextRO;
	private _time_txt:iTextRO;
	private _last_drawn_numbers_lbl_txt:iTextRO;
	// buttons
	private _settings_btn:iButtonRO;
	private _clear_all_bets_btn:iButtonRO;
	private _clear_last_bet_btn:iButtonRO;
	private _active_chip_btn:iButtonRO;
	private _active_chip_btn_icon:iGridChipRO;
	private _double_bet_btn:iButtonRO;
	private _repeate_bet_btn:iButtonRO;
	private _bet_btn:iButtonRO;
	private _switch_grid_type_to_circle_btn:iButtonRO;
	private _switch_grid_type_to_square_btn:iButtonRO;

	constructor(){
		super('bet');

		// *******************  create needed layers  *******************
		this.grid_l = new Layer( 'grid' );
		this.grid_chips_l = new Layer( 'grid_chips' );
		this.buttons_l = new Layer( 'buttons' );
		this.texts_l = new Layer( 'texts' );
		this.betting_chips_l = new Layer( 'betting_chips' );

		// *******************  add them to this page  *******************
		this.addLayer( this.grid_l );
		this.addLayer( this.grid_chips_l );
		this.addLayer( this.buttons_l );
		this.addLayer( this.texts_l );
		this.addLayer( this.betting_chips_l );

	}

	public init(){

		// ustvarimo vse potrebne elemente
		this.createRenderObjects();
		// izberemo želeni pogled za grid / tepih
		this.setGrid( GridType.SQUARE );	// TODO - kater grid prikažemo po default-u, beremo iz Config-a
		// določimo aktivni chip za stave
		this.setActiveChip( 0 );

		this.addGameManagerListeners();
		
		this.updateDataFromGamemanager();
		
		/*******************  POMOČ ZA RAZVOJ  *******************/
		let runTest:boolean = true;

		if( runTest ){
			setTimeout(() => {
				// nastavimo tip chip-a
				this.setActiveChip( 4 );

				// postavimo en čip
				// this.makeBet( 1 );

				// postavimo vse čipe
				for (var i = 1; i <= 192; ++i) {
					this.makeBet( i );
				}
				
				// sprožimo BET
				setTimeout( this.onBetBtnClick, 1000 );
				
				// odpremo bet chip select
				// this.onActiveChipBtnClick();
				
				// izberemo pogled z circle grid
				// this.setGrid( GridType.CIRCLE );

			}, 1000 );
		}
	}

	/**
	 * pripravimo vse elemente, ki jih bomo v igri uporabljali
	 */
	private createRenderObjects(){
		let am = this.am;
		let lm = this.lm;

		// *************************************************
		// *		BASES - START
		// *************************************************
		
		this._base_top = new ImageRO( am.get( "base_top" ) );
		this._base_top.x = 0;
		this._base_top.y = 0;
		this.grid_l.addRenderObject( this._base_top );

		this._base_bottom = new ImageRO( am.get( "base_bottom" ) );
		this._base_bottom.x = 0;
		this._base_bottom.y = Config.stage_height - 100;
		this.grid_l.addRenderObject( this._base_bottom );
		
		// *************************************************
		// *		BASES - END
		// *************************************************

		// *************************************************
		//		GRIDS - START
		// *************************************************
		
		// *******************  square grid  *******************
		this._grid_square = new ImageRO( am.get('betting_field_square') );
		this._grid_square.onClick = ( mouse_position:iMousePosition ) => this.onGridClick( mouse_position );
		this._grid_square.x = 0;
		this._grid_square.y = 50;	// TODO - beri iz CONFIGa

		let grid_square_colors_img = am.get( 'betting_field_square_colors' );

		let square_grid_canvas = document.createElement('canvas');
			square_grid_canvas.width = grid_square_colors_img.width;
			square_grid_canvas.height = grid_square_colors_img.height;
		this._grid_square_colors_ctx = square_grid_canvas.getContext('2d');
		this._grid_square_colors_ctx.drawImage( grid_square_colors_img, 0, 0, grid_square_colors_img.width, grid_square_colors_img.height );

		this._grid_square_keys = am.get('betting_field_square_keys');

		this.grid_l.addRenderObject( this._grid_square );


		// *******************  circle grid  *******************
		this._grid_circle = new ImageRO( am.get('betting_field_circle') );
		this._grid_circle.onClick = ( mouse_position:iMousePosition ) => this.onGridClick( mouse_position );
		this._grid_circle.x = 10;
		this._grid_circle.y = 40;	// TODO - beri iz CONFIGa

		let grid_circle_colors_img = am.get( 'betting_field_circle_colors' );

		let circle_grid_canvas = document.createElement('canvas');
			circle_grid_canvas.width = grid_circle_colors_img.width;
			circle_grid_canvas.height = grid_circle_colors_img.height;
		this._grid_circle_colors_ctx = circle_grid_canvas.getContext('2d');
		this._grid_circle_colors_ctx.drawImage( grid_circle_colors_img, 0, 0, grid_circle_colors_img.width, grid_circle_colors_img.height );

		this._grid_circle_keys = am.get('betting_field_circle_keys');

		this.grid_l.addRenderObject( this._grid_circle );
		
		// *************************************************
		//		GRIDS - END
		// *************************************************

		// *************************************************
		//		BETTING CHIPS - START
		// *************************************************
		
		this._betting_chips_bg = new ImageRO( am.get("base_bet_chips") );
		this._betting_chips_bg.centerX = Config.stage_width / 2;
		this._betting_chips_bg.centerY = 725;

		this.betting_chips_l.addRenderObject( this._betting_chips_bg );

		Config.bet_chips.forEach( (bet_chip:iConfigBetChip, chip_index ) => {
			let ro_active:iGridChipRO = new GridChipRO( am.get('bet_chip_off_'+chip_index), am.get('bet_chip_on_'+chip_index) );
				ro_active.value = bet_chip.value;
				ro_active.onClick = () => this.setActiveChip( chip_index );
				ro_active.x = this._betting_chips_bg.x + - 10 + ( ro_active.width - 35 ) * chip_index;
				ro_active.centerY = this._betting_chips_bg.centerY - 10;

			let chip_active:iBettingChip = new BettingChip();
				chip_active.value = bet_chip.value;
				chip_active.value = bet_chip.value;
				chip_active.render_object = ro_active;
			this._betting_chips.push( chip_active );
			this.betting_chips_l.addRenderObject( ro_active );
		});
		this.betting_chips_l.onClick = this.onActiveChipBtnClick;
		this.betting_chips_l.visible = false;
		
		// *************************************************
		//		BETTING CHIPS - END
		//*************************************************

		// *************************************************
		//		GRID CHIPS - START
		// *************************************************
		
		// TODO - tukaj bomo predpirpravili chip-e, ki bodo kasneje postavljeni na grid / pripravimo cache
		
		// *************************************************
		//		GRID CHIPS - END
		// *************************************************

		// *************************************************
		//		BUTTONS - START
		// *************************************************
		
		let button_row_y:number = 843;
		let button_img:any = am.get('button');
		let button_text_color

		// *******************  settings button  *******************
		this._settings_btn = new ButtonRO( button_img, "" );
		this._settings_btn.x = 20;
		this._settings_btn.y = button_row_y;
		this._settings_btn.icon = am.get('icon_settings');
		this._settings_btn.onClick = () => alert("NEED TODO");
		this.buttons_l.addRenderObject( this._settings_btn );

		// *******************  clear all  *******************
		this._clear_all_bets_btn = new ButtonRO( button_img, "CLEAR\nALL BETS" );
		this._clear_all_bets_btn.x = 310;
		this._clear_all_bets_btn.y = button_row_y;
		this._clear_all_bets_btn.icon = am.get('icon_clear_all_bets');
		this._clear_all_bets_btn.onClick = this.onClearAllBetsClick;
		this.buttons_l.addRenderObject( this._clear_all_bets_btn );

		// *******************  clear last bet  *******************
		this._clear_last_bet_btn = new ButtonRO( button_img, "CLEAR\nLAST BET" );
		this._clear_last_bet_btn.x = 600;
		this._clear_last_bet_btn.y = button_row_y;
		this._clear_last_bet_btn.icon = am.get('icon_clear_last_bet');
		this._clear_last_bet_btn.onClick = this.onClearLastBetClick;
		this.buttons_l.addRenderObject( this._clear_last_bet_btn );

		// *******************  active chip  *******************
		this._active_chip_btn = new ButtonRO( am.get('button_mini'), "" );
		this._active_chip_btn.x = 890;
		this._active_chip_btn.y = button_row_y;
		this._active_chip_btn.onClick = this.onActiveChipBtnClick;
		this.buttons_l.addRenderObject( this._active_chip_btn );

		// *******************  double bet  *******************
		this._double_bet_btn = new ButtonRO( button_img, "DOUBLE\nBET", am.get('button_kmet') );
		this._double_bet_btn.x = 1070;
		this._double_bet_btn.y = button_row_y;
		this._double_bet_btn.icon = am.get('icon_double_bet');
		this._double_bet_btn.onClick = this.onDoubleBetBtnClick;
		this.buttons_l.addRenderObject( this._double_bet_btn );

		// *******************  repeat bet  *******************
		this._repeate_bet_btn = new ButtonRO( button_img, "REPEAT\nBET" );
		this._repeate_bet_btn.x = 1360;
		this._repeate_bet_btn.y = button_row_y;
		this._repeate_bet_btn.icon = am.get('icon_repeat_bet');
		this._repeate_bet_btn.onClick = this.onRepeatBetBtnClick;
		this.buttons_l.addRenderObject( this._repeate_bet_btn );

		// *******************  bet  *******************
		this._bet_btn = new ButtonRO( button_img, "BET" );
		this._bet_btn.fontSize = 50;
		this._bet_btn.x = 1650;
		this._bet_btn.y = button_row_y;
		this._bet_btn.onClick = this.onBetBtnClick;
		this.buttons_l.addRenderObject( this._bet_btn );

		// *******************  switch grid  *******************
		this._switch_grid_type_to_circle_btn = new ButtonRO( am.get('switch_grid_to_circle_button'), "" );
		this._switch_grid_type_to_circle_btn.x = 1713;
		this._switch_grid_type_to_circle_btn.y = 638;
		this._switch_grid_type_to_circle_btn.onClick = this.onGridTypeBtnClick;
		this.buttons_l.addRenderObject( this._switch_grid_type_to_circle_btn );

		this._switch_grid_type_to_square_btn = new ButtonRO( am.get('switch_grid_to_square_button'), "" );
		this._switch_grid_type_to_square_btn.x = 1820;
		this._switch_grid_type_to_square_btn.y = 718;
		this._switch_grid_type_to_square_btn.onClick = this.onGridTypeBtnClick;
		this.buttons_l.addRenderObject( this._switch_grid_type_to_square_btn );
		
		// *************************************************
		//		BUTTONS - END
		// *************************************************
		
		// *************************************************
		//		TEXTS - START
		// *************************************************

		// *******************  total stake label  *******************
		this._total_stake_lbl_txt = new TextRO("TOTAL\nSTAKE");
		this._total_stake_lbl_txt.x = 20;
		this._total_stake_lbl_txt.y = 20;
		this.texts_l.addRenderObject( this._total_stake_lbl_txt );

		// *******************  total stake  *******************
		this._total_stake_txt = new TextRO("$ 15.000");
		this._total_stake_txt.textAlign = "right";
		this._total_stake_txt.x = 490;
		this._total_stake_txt.y = 32;
		this.texts_l.addRenderObject( this._total_stake_txt );

		// *******************  game status  *******************
		this._game_status_txt = new TextRO("GAME STATUS");
		this._game_status_txt.textAlign = "center";
		this._game_status_txt.x = 950;
		this._game_status_txt.y = 32;
		this.texts_l.addRenderObject( this._game_status_txt );

		// *******************  credits label  *******************
		this._credits_lbl_txt = new TextRO("CREDIT");
		this._credits_lbl_txt.x = 1400;
		this._credits_lbl_txt.y = 32;
		this.texts_l.addRenderObject( this._credits_lbl_txt );

		// *******************  credits  *******************
		this._credits_txt = new TextRO("$ ");
		this._credits_txt.textAlign = "right";
		this._credits_txt.x = 1880;
		this._credits_txt.y = 30;
		this.texts_l.addRenderObject( this._credits_txt );

		// *******************  time  *******************
		this._time_txt = new TextRO("12:34");
		this._time_txt.x = 35;
		this._time_txt.y = 1030;
		this.texts_l.addRenderObject( this._time_txt );

		// *******************  last drawn number label  *******************
		this._last_drawn_numbers_lbl_txt = new TextRO("SCROLL LAST\nDRAWN NUMBERS");
		this._last_drawn_numbers_lbl_txt.x = 1635;
		this._last_drawn_numbers_lbl_txt.y = 1000;
		this.texts_l.addRenderObject( this._last_drawn_numbers_lbl_txt );
		
		// *************************************************
		//		TEXTS - END
		// *************************************************

	}

	private addGameManagerListeners(){
		this.gm.onConnection.add( this.onConnectionChange );
		this.gm.onPayout.add( this.onPayout );
		this.gm.onBallShot.add( this.onBallShot );
		this.gm.onBetConfirm.add( this.onBetConfirm );
		this.gm.onBetReject.add( this.onBetReject );
		this.gm.onBetRemoveConfirmed.add( this.onBetRemoveConfirmed );
		this.gm.onBetRemoveRejected.add( this.onBetRemoveRejected );
		this.gm.onManualBallShotEnabled.add( this.onManualBallShotEnabled );
		this.gm.onUserCredits.add( this.onUserCredits );
		this.gm.onStake.add( this.onStake );
		this.gm.onWinNumber.add( this.onWinNumber );
		this.gm.onClearBets.add( this.onClearBets );
	}

	private updateDataFromGamemanager(){
		this.onConnectionChange();
		this.onPayout();
		this.onManualBallShotEnabled();
		this.onUserCredits();
		this.onStake();
	}

	/**
	 * Ko uporabnik klikne na grid / tepih
	 * @param {[type]} event [description]
	 */
	private onGridClick( mouse_position:iMousePosition ){

		// *******************  glede na tip grid-a pripravimo potrebne podatke  *******************
		
		let grid:iRenderObject;
		let grid_keys:iGridKeys;
		let grid_colors_ctx:any;

		switch ( this._grid_type_selected ) {
			case GridType.SQUARE:
				grid = this._grid_square;
				grid_keys = this._grid_square_keys;
				grid_colors_ctx = this._grid_square_colors_ctx;
				break;

			case GridType.CIRCLE:
				grid = this._grid_circle;
				grid_keys = this._grid_circle_keys;
				grid_colors_ctx = this._grid_circle_colors_ctx;
				break;
			
			default:
				throw new Error('BetPage::onGridClick() error. GridType unknown.');
		}

		// *******************  probamo dobiti bet field  *******************
		
		// get X and Y in real size grid
		var gridX:number = mouse_position.mouseX - grid.x;
		var gridY:number = mouse_position.mouseY - grid.y;

		// console.log( "onGridClick", gridX, gridY );

		// check point if matches grid field
		let grid_key_index:any = false;
		let grid_colors_color = grid_colors_ctx.getImageData(gridX, gridY, 1, 1).data;
		let blue_color_int = grid_colors_color[2];

		if( grid_keys.hasOwnProperty( blue_color_int ) ){
			grid_key_index = blue_color_int;
		}

		// *******************  če ga nismo našli, ne naredimo nič  *******************
		
		if( false === grid_key_index ){
			return;
		}

		// *******************  imamo id polja, zato sprožimo zahteveh za MAKE BET  *******************
		this.makeBet( parseInt( grid_key_index ) );
	}

	/**
	 * ustvarimo stavo
	 * @param {number} grid_key_index
	 */
	private makeBet( grid_key_index:number, from_grid_chip?:iGridChip ){

		let active_betting_chip_index = this.active_betting_chip_index;
		let double_bet_active:boolean = this._double_bet_btn.activated;

		// če smo posredovali če grid-chip, preberemo potrebne vrednosti iz njega
		if( from_grid_chip ){
			active_betting_chip_index = from_grid_chip.betting_chip_index;
			double_bet_active = from_grid_chip.double_bet;
		}

		// dobimo betting chip / vrednost, s katero želimo staviti
		let betting_chip:iBettingChip = this._betting_chips[ active_betting_chip_index ];
		let bet_value:number = betting_chip.value;
		

		// pripravimo žeton
		let grid_chip:iGridChip = new GridChip();
			grid_chip.grid_key_index = grid_key_index;
			grid_chip.betting_chip_index = active_betting_chip_index;
			grid_chip.value = betting_chip.value;
			grid_chip.state = ChipState.WAITING_CONFIRMATION;
			grid_chip.double_bet = double_bet_active;

		let grid_chip_bet_value:number = grid_chip.double_bet ? grid_chip.value * 2 : grid_chip.value;

		// preverimo če imamo pogoj, za min bet
		if( Config.game_min_bet > 0 && grid_chip_bet_value < Config.game_min_bet ){
			// če imamo kaj stav na tem mestu, lahko logično sklepamo, da smo ta pogoj že uredili
			if(
				( grid_key_index in this._grid_chips_by_grid_key ) &&
				this._grid_chips_by_grid_key[ grid_key_index ].filter(( tmp_grid_chip:iGridChip ) => { return ! tmp_grid_chip.delete; }).length > 0
				){
				// ne naredimo nič, ker že imamo stavo na grid_key-ju
			} else {
				// stavo spremenimo v višino vrednosti, ki je pogoj za minimalno stavo
				for (var i = 0; i < this._betting_chips.length; ++i) {
					let tmp_bc:iBettingChip = this._betting_chips[ i ];
					let tmp_bc_value:number = double_bet_active ? tmp_bc.value * 2 : tmp_bc.value;
					if( tmp_bc_value >= Config.game_min_bet ){
						// smo našli stavo, ki jo bomo poskušali postaviti na grid (če imamo dovolj denarja itd)
						grid_chip.betting_chip_index = i;
						grid_chip.value = tmp_bc.value;
						break;
					}
				}	
			}
		}

		// preverimo, če ta žeton lahko postavimo na mizo
		if( ! this.gm.canMakeBet( grid_chip ) ){
			// ne moremo postaviti te stave
			return;
		}

		// si shranimo stavo v seznam
		this._grid_chips.push( grid_chip );
		// si shranimo še glede na grid_key, da jih lažje postavimo
		if( ! ( grid_key_index in this._grid_chips_by_grid_key ) ){
			this._grid_chips_by_grid_key[ grid_key_index ] = [];
		}
		this._grid_chips_by_grid_key[ grid_key_index ].push( grid_chip );

		if( this._grid_chips_render_objects[ grid_key_index ] ){
			this.grid_chips_l.removeRenderObject( this._grid_chips_render_objects[ grid_key_index ] );
			delete this._grid_chips_render_objects[ grid_key_index ];
		}

		// prikažemo žetone na grid-u
		this.prepGridChipsOnGrid();

		// javimo igri, da želimo postaviti stavo, kar nam bo kasneje potrdilo ali zavrglo našo zahtevo za stavo
		this.gm.requestAddBet( grid_chip );
	}

	private prepGridChipsOnGrid(){
		let grid_ro:iRenderObject = ( this._grid_type_selected == GridType.SQUARE ? this._grid_square : this._grid_circle );
		let grid_keys:iGridKeys = ( this._grid_type_selected == GridType.SQUARE ? this._grid_square_keys : this._grid_circle_keys );

		for( let grid_key_index in this._grid_chips_by_grid_key ){
			let grid_key_chips:iGridChip[] = this._grid_chips_by_grid_key[ grid_key_index ];
			if( 0 == grid_key_chips.length ){
				continue;
			}

			// render object že obstaja, ne naredimo nič
			if( this._grid_chips_render_objects[ grid_key_index ] ){
				continue;
			}

			// render object še ne obstaja, zato ga pripravimo
			// poiščemo zadnji grid chip, ki ni označen za brisanje in prikažemo tega na grid-u
			for (var i = grid_key_chips.length - 1; i >= 0; i--) {
				let grid_chip:iGridChip = grid_key_chips[i];
				
				if( grid_chip.delete ){
					continue;
				}

				let ro:iGridChipRO = new GridChipRO( this.am.get('grid_chip_off_'+grid_chip.betting_chip_index), this.am.get('grid_chip_on_'+grid_chip.betting_chip_index) );
					ro.value = grid_chip.value;

				let grid_key:iGridKey = grid_keys[ grid_key_index ];
				if( grid_key && grid_key.centerX && grid_key.centerY ){
					ro.visible = true;
					ro.centerX = grid_ro.x + grid_key.centerX;
					ro.centerY = grid_ro.y + grid_key.centerY;
				} else {
					ro.visible = false;
				}
				ro.active = ( grid_chip.state == ChipState.CONFIRMED );

				this._grid_chips_render_objects[ grid_key_index ] = ro;

				this.grid_chips_l.addRenderObject( ro );

				break;
			}
		}

	}

	/******************************************************************
	*
	*       ON CLICK HANDLERS
	*
	******************************************************************/
	
	private onBetBtnClick = () => {
		if( 0 == this._grid_chips.filter( ( grid_chip:iGridChip ) => { return ! grid_chip.delete && grid_chip.state == ChipState.CONFIRMED; } ).length ){
			// imamo samo nepotrjene ali izbrisane stave / nimamo uporabnih stav, zato ne naredimo nič
			return;
		}

		this.um.switchPage( this.lm.getPage('circle') );
		this.gm.makeBallShot();
	};

	private onGridTypeBtnClick = () => {
		if( this._grid_type_selected == GridType.SQUARE ){
			this.setGrid( GridType.CIRCLE );
			return;
		}
		if( this._grid_type_selected == GridType.CIRCLE ){
			this.setGrid( GridType.SQUARE );
			return;
		}

		this.setGrid( GridType.SQUARE );
	};

	private onClearAllBetsClick = () => {
		if( 0 == this._grid_chips.length ){
			return;
		}

		this._grid_chips.forEach( ( grid_chip:iGridChip ) => {
			if( grid_chip.delete ){
				return;
			}

			if( ! this.gm.canRemoveBet( grid_chip ) ){
				return;
			}

			// označimo za brisanje
			grid_chip.delete = true;

			// ga odstranimo iz prikaza na grid-u
			let grid_chip_ro:iGridChipRO = this._grid_chips_render_objects[ grid_chip.grid_key_index ];
			if( grid_chip_ro ){
				this.grid_chips_l.removeRenderObject( grid_chip_ro );
				delete this._grid_chips_render_objects[ grid_chip.grid_key_index ];
			}

			// sprožimo zahtevo za odstranitev stave
			this.gm.requestRemoveBet( grid_chip );

		} );

		// sprožimo ponoven izris chip-ov na grid-u
		this.prepGridChipsOnGrid();
	};

	private onClearLastBetClick = () => {
		if( 0 == this._grid_chips.length ){
			return;
		}

		// najdemo zadnji grid chip / stavo, ki jo želimo odstraniti
		let remove_grid_chip:iGridChip;
		for (var i = this._grid_chips.length - 1; i >= 0; i--) {
			let grid_chip:iGridChip = this._grid_chips[i];
			// preskočimo chip-e, ki so že označeni za brisanje
			if( grid_chip.delete ){
				continue;
			}
			if( ChipState.WAITING_CONFIRMATION == grid_chip.state || ChipState.CONFIRMED == grid_chip.state ){
				// smo našli naslednji chip, ki ga lahko odstranimo
				remove_grid_chip = grid_chip;
				break;
			}
		}

		// nismo načli chip-a, ki bo ga lahko odstranili
		if( ! remove_grid_chip ){
			return;
		}

		// če nam igra ne dovoljuje odstranitev stave
		if( ! this.gm.canRemoveBet( remove_grid_chip ) ){
			return;
		}

		// spremenimo status
		remove_grid_chip.delete = true;

		// ga odstranimo iz prikaza na grid-u
		let grid_chip_ro:iGridChipRO = this._grid_chips_render_objects[ remove_grid_chip.grid_key_index ];
		if( grid_chip_ro ){
			this.grid_chips_l.removeRenderObject( grid_chip_ro );
			delete this._grid_chips_render_objects[ remove_grid_chip.grid_key_index ];
		}

		// sprožimo prikaz naslednjega / prejšnjega chip-a na grid-u
		this.prepGridChipsOnGrid();

		// sprožimo zahtevo za odstranitev stave
		this.gm.requestRemoveBet( remove_grid_chip );
	};

	private onActiveChipBtnClick = () => {
		this.betting_chips_l.visible = ! this.betting_chips_l.visible;
	};

	private onDoubleBetBtnClick = () => {
		this._double_bet_btn.activated = ! this._double_bet_btn.activated;
	};

	private onRepeatBetBtnClick = () => {
		let previous_bets:iGridChip[] = this.gm.getPreviousBets();
		for (var i = 0; i < previous_bets.length; ++i) {
			let grid_chip:iGridChip = previous_bets[i];
			this.makeBet( grid_chip.grid_key_index, grid_chip );
		}
	};

	/******************************************************************
	*
	*       SET GRID
	*
	******************************************************************/

	private setGrid( type:GridType ){

		this._grid_type_selected = type;

		this._grid_square.visible = ( type == GridType.SQUARE );
		this._grid_circle.visible = ( type == GridType.CIRCLE );
		
		this._switch_grid_type_to_square_btn.visible = ( type == GridType.CIRCLE );
		this._switch_grid_type_to_circle_btn.visible = ( type == GridType.SQUARE );

		let grid_ro:iRenderObject = ( this._grid_type_selected == GridType.SQUARE ? this._grid_square : this._grid_circle );
		let grid_keys:iGridKeys = ( this._grid_type_selected == GridType.SQUARE ? this._grid_square_keys : this._grid_circle_keys);


		// ponovno postavimo žetone, ki so na mizi
		for( let grid_key_index in this._grid_chips_render_objects ){
			if( ! this._grid_chips_render_objects[ grid_key_index ] ){
				continue;
			}
			
			let ro:iGridChipRO = this._grid_chips_render_objects[ grid_key_index ];

			let grid_key:iGridKey = grid_keys[ grid_key_index ];
			
			if( grid_key ){
				ro.visible = true;
				ro.centerX = grid_ro.x + grid_key.centerX;
				ro.centerY = grid_ro.y + grid_key.centerY;
			} else {
				ro.visible = false;
			}
		}
	}

	/**
	 * izberemo či, s katerim bomo stavili v igri
	 * @param {number} index
	 */
	private setActiveChip( index:number ){
		if( 'undefined' === typeof this._betting_chips[index] ){
			return;
		}

		// izberemo žeton
		let active_chip_value:number;
		for( var chip_index = 0; chip_index < this._betting_chips.length; chip_index++ ){
			let chip:iBettingChip = this._betting_chips[ chip_index ];
			let ro:iGridChipRO = chip.render_object;
			ro.active = ( index == chip_index ) ? true : false;

			if( ro.active ){
				active_chip_value = chip.value;
			}
		}

		this.active_betting_chip_index = index;


		if( this._active_chip_btn_icon ){
			this.buttons_l.removeRenderObject( this._active_chip_btn_icon );
		}

		this._active_chip_btn_icon = new GridChipRO( this.am.get('grid_chip_on_' + this.active_betting_chip_index ), this.am.get('grid_chip_off_' + this.active_betting_chip_index ) );
		this._active_chip_btn_icon.value = active_chip_value;
		this._active_chip_btn_icon.centerX = this._active_chip_btn.x + this._active_chip_btn.width / 2;
		this._active_chip_btn_icon.centerY = this._active_chip_btn.y + this._active_chip_btn.height / 2;
		
		this.buttons_l.addRenderObject( this._active_chip_btn_icon );

		this.betting_chips_l.visible = false;
	}

	// ******************************************************************
	// *
	// *       ON GAME EVENTS
	// *
	// ******************************************************************
	
	private onConnectionChange = () => {
		let status:GameConnectionStatus = this.gm.getConnectionStatus();
		if( status == GameConnectionStatus.LOST ){
			this._game_status_txt.text = "NO CONNECTION";
		}
	};

	private onPayout = () => {
		let payout:number = this.gm.getPayout();
		if( payout > 0 ){
			// TODO
		}
	};

	private onBallShot = () => {
		this._game_status_txt.text = "Ball shot";
	};

	private onBetConfirm = ( grid_chip:iGridChip ) => {
		let grid_key_chips:iGridChip[] = this._grid_chips_by_grid_key[ grid_chip.grid_key_index ];
		for (var i = grid_key_chips.length - 1; i >= 0; i--) {
			let last_chip:iGridChip = grid_key_chips[i];
			// na "vrhu" imamo lahko postavljene chip-e, ki se ne vidijo, ker so označeni za izbris, ampak izbris še ni bil potrjen
			// zato nadaljujemo na naslednji žeton, ki pa ni označen za izbris
			if( last_chip.delete ){
				continue;
			}
			// če naslednji žeton, ki ni označen za izbris, ni žeton, ki je bil ravnokar potrjen, ne naredimo nič, ker
			// želimo žeton narediti aktiven le za zadnji chip / vrhnji žeton
			if( last_chip != grid_chip ){
				break;
			}
			this._grid_chips_render_objects[ grid_chip.grid_key_index ].active = true;
			break;
		}
	};

	private onBetReject = ( grid_chip:iGridChip ) => {
		
		// odstranimo chip iz nabora chip-ov
		let index1:number = this._grid_chips.indexOf( grid_chip );
		if( -1 != index1 ){
			this._grid_chips.splice( index1, 1 );
		}

		// odstranimo chip iz drugega nabora chip-ov
		let index2:number = this._grid_chips_by_grid_key[ grid_chip.grid_key_index ].indexOf( grid_chip );
		if( -1 != index2 ){
			this._grid_chips_by_grid_key[ grid_chip.grid_key_index ].splice( index2, 1 );
		}

		// če smo ta chip izrisovali, ga odstranimo
		if( this._grid_chips_render_objects[ grid_chip.grid_key_index ] ){
			this.grid_chips_l.removeRenderObject( this._grid_chips_render_objects[ grid_chip.grid_key_index ] );
			delete this._grid_chips_render_objects[ grid_chip.grid_key_index ];
		}

		// zahtevamo izris chip-ov na grid-u, ker smo ta chip "odstranili" in je potrebno (po potrebi) prikazati naslednjega, ki sotji na istem mestu
		this.prepGridChipsOnGrid();
	};

	private onBetRemoveConfirmed = ( grid_chip:iGridChip ) => {
		// odstranimo iz prvega nabora
		let grid_chip_index = this._grid_chips.indexOf( grid_chip );
		if( grid_chip_index ){
			this._grid_chips.splice( grid_chip_index, 1 );
		}

		// odstranimo iz drugega nabora
		let grid_chip_index2 = this._grid_chips_by_grid_key[ grid_chip.grid_key_index ].indexOf( grid_chip );
		if( grid_chip_index2 ){
			this._grid_chips_by_grid_key[ grid_chip.grid_key_index ].splice( grid_chip_index2, 1 );
		}
	};

	private onBetRemoveRejected = ( grid_chip:iGridChip ) => {
		let grid_key_chips:iGridChip[] = this._grid_chips_by_grid_key[ grid_chip.grid_key_index ];
		for (var i = grid_key_chips.length - 1; i >= 0; i--) {
			let last_chip:iGridChip = grid_key_chips[i];
			// na "vrhu" imamo lahko postavljene chip-e, ki se ne vidijo, ker so označeni za izbris, ampak izbris še ni bil potrjen
			// zato nadaljujemo na naslednji žeton, ki pa ni označen za izbris
			if( last_chip.delete ){
				continue;
			}
			// če naslednji žeton, ki ni označen za izbris, ni žeton, ki je bil ravnokar potrjen, ne naredimo nič, ker
			// želimo žeton narediti aktiven le za zadnji chip / vrhnji žeton
			if( last_chip != grid_chip ){
				break;
			}

			let grid_chip_ro:iGridChipRO = this._grid_chips_render_objects[ grid_chip.grid_key_index ];
			if( grid_chip_ro ){
				this.grid_chips_l.removeRenderObject( grid_chip_ro );
				delete this._grid_chips_render_objects[ grid_chip.grid_key_index ];
				this.prepGridChipsOnGrid();
			}
			break;
		}
	};

	private onManualBallShotEnabled = () => {
		let enabled:boolean = this.gm.getManualBallShotEnabled();
		this._bet_btn.visible = enabled;
	};

	private onUserCredits = () => {
		let credits = this.gm.getUserCredits();
		let currency_lbl = "$";	// TODO - beri od nekod
		this._credits_txt.text = currency_lbl + " " + credits;
	};

	private onStake = () => {
		let stake:number = this.gm.getStake();
		let currency_lbl = "$";	// TODO - beri od nekod
		this._total_stake_txt.text = currency_lbl + " " + stake;
	};

	private onWinNumber = () => {
		// izžrebana je bila številka, počistimo vse stave na mizi
		this.grid_chips_l.removeAllRenderObjects();
		this._grid_chips = [];
		for( let grid_kex_index in this._grid_chips_by_grid_key ){
			this._grid_chips_by_grid_key[ grid_kex_index ] = [];
		}
	};

	private onClearBets = () => {
		this.grid_chips_l.removeAllRenderObjects();
		this._grid_chips = [];
		this._grid_chips_by_grid_key = {};
	};

}