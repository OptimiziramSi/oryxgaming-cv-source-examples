import { Component, ViewChild } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RouletteGame } from '../../directives/roulette-game/roulette-game';

@Component({
  templateUrl: 'build/pages/roulette/roulette.html',
  directives: [RouletteGame],
})
export class RoulettePage {

	@ViewChild('game') roulette_game: RouletteGame;
	
	constructor(public navCtrl: NavController) {

	}

	ionViewWillEnter(){
		this.roulette_game.initFromJson( "./assets/directives/roulette-game/configs/roulette-game.json" );

		this.roulette_game.start();
	}

	ionViewDidEnter(){
		this.roulette_game.resizeGame();
	}

	ionViewWillLeave(){
		this.roulette_game.pause();
		this.roulette_game.destroy();
	}



}
