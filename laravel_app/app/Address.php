<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\Helper;
use App\Nancy\NancyClient;

class Address extends Model {

    protected static $NancyTypeName = 'XGeNaslovnik';
    protected static $NancySort = 'Naslovnik_1';
    protected static $NancyFillable_map = [
        "id" => "Id",
        "company_id" => "PartnerId",
        "addressee_1" => "Naslovnik_1",
        "addressee_2" => "Naslovnik_2",
        "street_1" => "Naslov_1",
        "street_2" => "Naslov_2",
        "post_number" => "Pošta",
        "city" => "Kraj",
        "country_id" => "DržavaId",
        "country_display_name" => "DržavaIme",
        "contact_name" => "Ime",
        "contact_phone" => "Telefon",
        "updated" => "ZadnjiDogodek",
        "active" => "Aktivno",
    ];
    protected static $NancyEditableFields = [
        "addressee_1",
        "addressee_2",
        "street_1",
        "street_2",
        "post_number",
        "city",
        "country_id",
        "contact_name",
        "contact_phone",
        "active",
    ];

    protected static $NancyUpdateIgnoreFields = [
        "company_id",
    ];

    protected static $NancyUpdateRequiredFields = [
        "id",
    ];

    protected $fillable = [
        "id",
        "partner_id",
        "company_id",
        "addressee_1",
        "addressee_2",
        "street_1",
        "street_2",
        "post_number",
        "city",
        "country_id",
        "country_display_name",
        "contact_name",
        "contact_phone",
        "updated",
        "active",
    ];

    protected $hidden = [];

    protected $guarded = [];

    protected $casts = [
        'active' => 'bool',
    ];

    private static $NancyDefaultCritria = null;
    protected static function defaultCriteria(){
        if(is_null(self::$NancyDefaultCritria)){
            if(Partner::current()){
                self::$NancyDefaultCritria = sprintf("PartnerId='%s'", Partner::current()->company_id);
            } else {
                self::$NancyDefaultCritria = "1=0";
            }
        }
        return self::$NancyDefaultCritria;
    }
}