<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\Helper;
use App\Nancy\NancyClient;

class Country extends Model {

    // začasno uporabljamo stari klic, ker je z novim nekaj narobe / vrača status 400
    // novi klic
    protected static $NancyTypeName = 'XGeDržava';
    protected static $NancySort = 'Ime';
    protected static $NancyFillable_map = [
        "id" => "Id",
        "display_name" => "Ime",
        "post_number_pattern" => "MaskaPošte",
    ];

	protected $fillable = [
        "id",
        "display_name",
        "post_number_pattern",
	];

	protected $hidden = [];

    protected $guarded = [];

    protected $casts = [];
}