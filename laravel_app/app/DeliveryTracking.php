<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\Helper;
use App\Nancy\NancyClient;

class DeliveryTracking extends Model {

    // začasno uporabljamo stari klic, ker je z novim nekaj narobe / vrača status 400
    // novi klic
    protected static $NancyTypeName = 'XGeNaročiloSledenje';
    protected static $NancySort = 'Datum descending';
    protected static $NancyFillable_map = [
        "updated" => "Datum",
        "location" => "Lokacija",
        "description" => "Opis",
    ];

	protected $fillable = [
        "updated",
        "location",
        "description",
	];

	protected $hidden = [];

    protected $guarded = [];

    protected $casts = [];

    public static function tracking( $delivery_id ){
        $url = self::NancyUrl(static::LIST_METHOD);
        $data = [ 'id' => $delivery_id ];

        $nancy_result = NancyClient::post($url, $data);

        if($nancy_result->status(200) && $nancy_result->json() && is_array($nancy_result->json())){
            $ret = [];
            foreach ($nancy_result->json() as $data_obj) {
                $model = new static(static::map_data(static::$NancyFillable_map, $data_obj));
                $model->delivery_id = $delivery_id;
                $ret[] = $model;
            }
            return $ret;
        }

        return [];
    }
}