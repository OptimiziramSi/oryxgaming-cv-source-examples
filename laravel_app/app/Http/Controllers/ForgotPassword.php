<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Mail;
use App\Partner;
use App\PartnerProperties;

class ForgotPassword extends Controller
{
    
	public function forgotpassword(){
		if(Partner::current()){
			return redirect(route('delivery_new'));
		}

		return view('pages.forgotpassword');
	}

	public function forgotpassword_submit(Request $request){
		if(Partner::current()){
			return redirect(route('delivery_new'));
		}

		$username = $request->input('username');
		$username = @strtoupper($username);

		if(!empty($username)){
			$partner = Partner::firstOrNull(sprintf("oznaka='%s'", $username ));
			if($partner && ! empty($partner->email)){

				$lostpassword_token = $partner->generateLostPasswordToken();

				$url = route('resetpassword', ['token' => $lostpassword_token]);

				Mail::send(['text' => 'emails.forgotpassword'], ['partner' => $partner, 'url' => $url], function ($mail) use ($partner, $url) {
					$mail->from('miha.puzelj@global-express.si', 'Global Express E-tovorni list');
					$mail->to($partner->email, $partner->display_name)->subject('Global Express - ponastavitev gesla');
				});

				return redirect(route('forgotpassword'))->with(['success' => true]);
			}
		}

		// partner not found
		return redirect(route('forgotpassword'))->with(['error' => 'Vpisano uporabniško ime ne obstaja ali pa za to uporabniško nimate nastavljenega email-a. Prosimo preverite, da je vpisano uporabniško ime pravilno pravilno in ponovno kliknite na "POŠLJI POVEZAVO ZA PONASTAVITEV GESLA". Če se vam to sporočilo ponovi, za ponastavitev gesla prosimo kontaktirajte Global Express.', 'username' => $username])->withInput();
	}

	public function resetpassword($token){
		$token_info = $this->check_token($token);
		if($token_info->error){
			return view('pages.resetpassword', ['token_error' => $token_info->error]);
		}

		return view('pages.resetpassword', ['token_error' => '', 'partner' => $token_info->partner]);
	}

	public function resetpassword_submit(Request $request, $token){
		$token_info = $this->check_token($token);
		if($token_info->error){
			return view('pages.resetpassword', ['token_error' => $token_info->error]);
		}

		$new_password = $request->input('new_password');
		$new_password2 = $request->input('new_password2');

		if( ! empty($new_password) && ! empty($new_password2) ){
			if( \Hash::check( $new_password, \Hash::make($new_password2) ) ){
				$partner = $token_info->partner;
				$partner->password = $new_password;
				if( ! $partner->update() ){
					return redirect(route('resetpassword', ['token' => $token]))->with(['error' => 'Pri posodabljanju je prišlo do težave. Prosimo poskusite ponovno kasneje.']);
    			} else {
    				$partnerProperties = $token_info->partnerProperties;
    				$partnerProperties->lostpassword_token = '';
    				$partnerProperties->lostpassword_created_at = 0;
    				$partnerProperties->save();

    				return redirect(route('resetpassword', ['token' => $token]))->with(['success' => true]);
    			}
			} else {
				return redirect(route('resetpassword', ['token' => $token]))->with(['error' => 'Gesli se ne ujemata.']);
			}
		} else {
			return redirect(route('resetpassword', ['token' => $token]))->with(['error' => 'Polji za novo geslo sta zahtevani.']);
		}
	}

	private function check_token($token){
		$token_info = (object)[
			'error' => false,
			'partnerProperties' => null,
			'partner' => null,
		];
		if(empty($token) || ! is_string($token) || strlen($token) < 100){
			$token_info->error = 'token_not_found';
			return $token_info;
		}

		$partnerPropertiesModel = PartnerProperties::where(['lostpassword_token' => $token])->first();

		if(!$partnerPropertiesModel){
			$token_info->error = 'token_not_found';
			return $token_info;
		}

		$valid_timespan = 30 * 60;	// 30 min
		if( (int)$partnerPropertiesModel->lostpassword_created_at + $valid_timespan < time() ){
			$token_info->error = 'not_valid_timespan';
			return $token_info;
		}

		$partner = Partner::firstOrNull(sprintf("Id='%s'", $partnerPropertiesModel->partner_id));
		if(!$partner){
			$token_info->error = 'partner_not_found';
			return $token_info;
		}

		$token_info->partnerProperties = $partnerPropertiesModel;
		$token_info->partner = $partner;

		return $token_info;
	}


}
