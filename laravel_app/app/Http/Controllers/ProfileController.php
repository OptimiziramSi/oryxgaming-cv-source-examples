<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Partner;

class ProfileController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index(){
        return view('pages.profile')
        	->with('email', Partner::current()->email);
    }
    
    public function update(Request $request){
    	$action = $request->input('action');
    	$data = [
    		'error' => '',
    	];

    	$partner = Partner::current();

    	switch ($action) {
            case 'news_read':
                $type = $request->input('type');
                if(!in_array($type, ['news','help'])){
                    return response()->json($data);
                }
                Partner::current()->prop_save($type . '_read_id', (int)$request->input('read_id'));
            break;
            case 'guide_viewed':
                $guide_code = $request->input('guide_code');
                $viewed_guides = $partner->prop('viewed_guides', []);
                $viewed_guides[] = $guide_code;
                $partner->prop_save('viewed_guides', $viewed_guides);
            break;
    		case 'update_email':
    			$partner->email = $request->input('email');
    			if( ! $partner->update() ){
    				$data['error'] = 'Pri posodabljanju je prišlo do težave. Prosimo poskusite ponovno kasneje.';
    			}
    			break;

    		case 'update_password':
    			$old_password = $request->input('old_password');
    			$new_password = $request->input('new_password');
    			$new_password2 = $request->input('new_password2');

    			if( \Hash::check( $old_password, \Hash::make($partner->password) ) ){
    				if( ! empty($new_password) && ! empty($new_password2) ){
    					if( \Hash::check( $new_password, \Hash::make($new_password2) ) ){
    						$partner->password = $new_password;
    						if( ! $partner->update() ){
			    				$data['error'] = 'Pri posodabljanju je prišlo do težave. Prosimo poskusite ponovno kasneje.';
			    			}
    					}
    				} else {
    					$data['error'] = 'Polji za novo geslo sta zahtevani.';
    				}
    			} else {
    				$data['error'] = 'Vpisano trenutno geslo ni pravilno.';
    			}
    			break;
    		
    		default:
    			$data['error'] = 'Zahtevana akcija ne obstaja.';
    			break;
    	}


        return response()->json($data);
    }
}
