<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Delivery;
use App\DeliveryTracking;
use App;

class TrackingController extends Controller
{
    public function __construct(){
    	// $this->middleware('auth');
    }

    public function index( $tracking_id, $lang ){
        $tracking_data = null;

        $this->set_locale( $lang );

        $delivery = $this->get_delivery_by_tracking_id( $tracking_id );
        if( ! is_null( $delivery ) ){
            $tracking_data = $this->get_tracking_data_from_delivery( $delivery );
        }

        return view('pages.tracking', [
            'delivery' => $tracking_data,

    		'no_header_menu' => true,
    		'no_footer_content' => true,
    		]);
    }

    public function json(Request $request){
        $tracking_data = null;

        if( ! $this->validate_gextracking_api_key( $request->header('gextracking-key') ) ){
            return response()->json(["error" => "gextracking_api_key_error"]);
        }

        $delivery_code = $request->input("code");
        $response_lang = $request->input("lang");

        $this->set_locale( $response_lang );

        $delivery = $this->find_delivery_by_code( $delivery_code );
        if( ! is_null( $delivery ) ){
            $tracking_data = $this->get_tracking_data_from_delivery( $delivery );

            // set date in locale
            foreach ($tracking_data[ 'tracking' ] as $tracking_idx => $tracking_obj) {
                $tracking_data[ 'tracking' ][ $tracking_idx ]['updated'] = date( trans('tracking.date_format'), strtotime( $tracking_obj->updated ) );
            }
        }

        return response()->json(["package" => $tracking_data]);
    }

    private function validate_gextracking_api_key( $key ){
        return ( $key === env("GEXTRACKING_API_KEY") );
    }

    private function set_locale( $lang ){
        if( ! in_array( $lang, ['sl', 'en'] ) )
            $lang = 'en';

        App::setLocale($lang);
    }

    private function find_delivery_by_code( $delivery_code ){
        $delivery = null;

        Delivery::$ignoreDefaultCriteria = true;

        // search by "code" field
        if( is_null( $delivery ) ){
            $delivery = Delivery::firstOrNull(sprintf("%s='%s'", Delivery::getNancyFieldsMap('code'), $delivery_code));

            // check found delivery code field match
            if( ! is_null( $delivery ) && $delivery_code != $delivery->code ){
                $delivery = null;
            }
        }

        // search by "tracking_awb" field
        if( is_null( $delivery ) ){
            $delivery = Delivery::firstOrNull(sprintf("%s='%s'", Delivery::getNancyFieldsMap('tracking_awb'), $delivery_code));

            // check found delivery code field match
            if( ! is_null( $delivery ) && $delivery_code != $delivery->tracking_awb ){
                $delivery = null;
            }
        }

        Delivery::$ignoreDefaultCriteria = false;

        return $delivery;
    }

    private function get_delivery_by_tracking_id( $tracking_id ){
        $delivery_code = @base64_decode( $tracking_id );
        if( ! $delivery_code )
            return null;

        Delivery::$ignoreDefaultCriteria = true;
        
        $delivery = Delivery::firstOrNull(sprintf("%s='%s'", Delivery::getNancyFieldsMap('code'), $delivery_code));
        
        Delivery::$ignoreDefaultCriteria = false;
        
        return $delivery;
    }

    private function get_tracking_data_from_delivery( Delivery $delivery ){
        $delivery_data = null;

        if( ! $delivery )
            return $delivery_data;
        
        $delivery_data = [
            'code' => $delivery->code,
            'courier_code' => $delivery->tracking_courier_code ? $delivery->tracking_courier_code : '/',
            'courier_display_name' => $delivery->tracking_courier_display_name ? $delivery->tracking_courier_display_name : '/',
            'courier_awb' => $delivery->tracking_awb ? $delivery->tracking_awb : '/',
            'tracking' => [],
        ];

        $delivery_tracking = DeliveryTracking::tracking( $delivery->id );
        if( is_array( $delivery_tracking ) )
            $delivery_data[ 'tracking' ] = $delivery_tracking;

        foreach ($delivery_data[ 'tracking' ] as $tracking_idx => $tracking_obj) {
            if( 'Global Express' == $tracking_obj->description ){
                $gex_status = intval( $tracking_obj->location );
                if( in_array( $gex_status, [ 10, 20, 30, 40, 50, 55, 60 , 70, 80, 90 ] ) ){
                    $tracking_obj->location = trans('tracking.gex_status_' . $gex_status);
                    $tracking_obj->description = trans('tracking.gex_status_' . $gex_status . '_description');

                    $delivery_data[ 'tracking' ][ $tracking_idx ] = $tracking_obj;
                }
            }
        }

        return $delivery_data;
    }
}
