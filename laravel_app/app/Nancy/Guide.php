<?php

namespace App\Nancy;

class Guide {
	
	public static function RawJsonEncode($input) {
		return preg_replace_callback(
			'/\\\\u([0-9a-zA-Z]{4})/',
			function ($matches) {
				return mb_convert_encoding(pack('H*',$matches[1]),'UTF-8','UTF-16');
			},
			json_encode($input)
		);
	}

	public static function sort_by_object_key(array &$arr, $key, $is_asc = true) {
		$sort_mul = $is_asc ? 1 : -1;
		usort($arr, function($a, $b) use ($key, $sort_mul) {
			if($a->{$key} && $b->{$key}){
				return $sort_mul * strcmp($a->{$key}, $b->{$key});
			}

			if(isset($a->{$key})){
				return $sort_mul * 1;
			}

			if(isset($b->{$key})){
				return $sort_mul * -1;
			}

			return 0;
		});
	}

}