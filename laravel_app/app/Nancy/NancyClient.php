<?php

namespace App\Nancy;

use App\Nancy\NancyClientException;

class NancyClient {
	
	public static function get( $url, $get_args = null ){
		return self::c($url, 'GET', $get_args, null);
	}

	public static function post( $url, $post_args = null, $get_args = null ){
		return self::c($url, 'POST', $get_args, $post_args);
	}

	private static function c( $url, $method, $get_args, $post_args ){
		return new self( env('NANCY_URL'), env('NANCY_KEY'), env('NANCY_SECRET'), $url, $method, $get_args, $post_args );
	}



	private $host;
	private $key;
	private $secret;

	private $url;
	private $method;
	private $get_args;
	private $post_args;

	private $status;
	private $raw;
	private $json_decoded;
	private $error;
	private $errorno;

	private function __construct( $host, $key, $secret, $url, $method, $get_args, $post_args ){
		$this->host = $host;
		$this->key = $key;
		$this->secret = $secret;

		$this->url = $url;
		$this->method = ($method == 'POST' ? 'POST' : 'GET');
		$this->get_args = is_array($get_args) ? $get_args : array();
		$this->post_args = is_array($post_args) ? $post_args : array();

		$this->call();
	}

	private function call(){
		$ch = curl_init();

		$url = rtrim($this->host, '/') . '/' . ltrim($this->url, '/');
		if (count($this->get_args)) {
			$params = http_build_query($this->get_args, '', '&amp;', PHP_QUERY_RFC3986);
			$url = $url . '?' . $params;
		}
		// print($url . "<br>");
		curl_setopt($ch, CURLOPT_URL, $url);

		// login
		curl_setopt($ch, CURLOPT_USERPWD, $this->key . ":" . $this->secret);

		if (count($this->post_args)) {
			curl_setopt($ch, CURLOPT_POST, 1);

			// use http_build_query to send as application/x-www-form-urlencoded
			curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($this->post_args, '', '&amp;'/*, PHP_QUERY_RFC3986*/));
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		}

		// set a generic user agent string
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.57 Safari/537.17');

		// allways return data
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		// connection establishing timeout and call timeout
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($ch, CURLOPT_TIMEOUT, 20);
		
		// if using secure connection, don't verify certificates
		if( substr($url, 4, 1) == 's' )
		{
			// CURLOPT_SSLVERSION
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		}

		curl_setopt($ch, CURLOPT_HEADER, 0);

		$this->raw = curl_exec($ch);
		$this->json_decoded = @json_decode($this->raw);
		$this->error = curl_error($ch);
		$this->errorno = curl_errno($ch);
		$this->status = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);

		if($this->errorno != 0){
			throw new NancyClientException('Nancy Api Had Errors');
		}
	}

	public function status($equals = null){
		if( ! is_null($equals) ){
			return ($equals === $this->status);
		}
		return $this->status;
	}

	public function content( $equals = null ){
		if( ! is_null($equals) ){
			return ($equals === $this->raw);
		}
		return $this->raw;
	}

	public function json(){
		return $this->json_decoded;
	}

	private function errorno($equals = null){
		if( ! is_null($equals) ){
			return ($equals === $this->errorno);
		}
		return $this->errorno;
	}
}