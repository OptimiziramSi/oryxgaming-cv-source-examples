<?php

namespace App\Nancy;

use Arr;
use App\User;
use App\Partner;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class NancyUserProvider implements UserProvider
{
    public function __construct()
    {

    }

    public function retrieveById($identifier)
    {
        if (is_array($identifier)) {
            $identifier = Arr::get($identifier, 0);
        }

        return $this->createUserModel($identifier);
    }

    public function retrieveByToken($identifier, $token)
    {
        return $this->createUserModel($identifier);
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        $user->setRememberToken($token);
    }

    public function retrieveByCredentials(array $credentials)
    {
        if( isset($credentials['username'], $credentials['password']) ){
            return $this->createUserModel($credentials['username'], $credentials['password']);
        }

        return null;
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        $username = $user->getAuthIdentifier();
        $username = @strtoupper($username);
        if(!$username){
            return false;
        }
        $partner = Partner::firstOrNull(sprintf("Oznaka='%s'", $username ) );

        if(is_null($partner)){
            return false;
        }

        if(!empty($partner->password) && \Hash::check($partner->password, $user->getAuthPassword())){
            return true;
        }

        return false;
    }

    private function createUserModel( $username, $password = null ){
        if(empty($username)){
            return null;
        }

        $user = new \App\User([
            'username' => $username,
            'password' => ($password) ? \Hash::make($password) : null,
        ]);

        return $user;
    }
}
