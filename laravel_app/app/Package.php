<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\NancyClient;

class Package extends Model {

	protected static $NancyTypeName = 'XGxPošiljkaPaket';
    protected static $NancySort = 'id+';
    protected static $NancyCriteria = [];
    protected static $NancyFillable_map = [
		"id" => "id",
		"posiljka_Id" => "pošiljka.Id",
		"teza" => "teža",
		"dolzina" => "dolžina",
		"sirina" => "širina",
		"visina" => "višina",
    ];

	protected $fillable = [
        "id",
		"posiljka_Id",
		"teza",
		"dolzina",
		"sirina",
		"visina",
	];

	protected $hidden = [];

    protected $guarded = [];

    protected $casts = [];

    public function save(){
        
    }

}