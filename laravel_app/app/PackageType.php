<?php

namespace App;

use App\Nancy\Model;
use App\Nancy\Helper;
use App\Nancy\NancyClient;

class PackageType extends Model {

    // začasno uporabljamo stari klic, ker je z novim nekaj narobe / vrača status 400
    // novi klic
    protected static $NancyTypeName = 'XGeVrstaPošiljke';
    protected static $NancySort = 'Ime';
    protected static $NancyFillable_map = [
        "id" => "Id",
        "display_name" => "Ime",
        "has_dimension" => "ImaDimenzije",
        "has_weight" => "ImaTežo",
        "active" => "Aktivno",
    ];

	protected $fillable = [
        "id",
        "display_name",
        "has_dimension",
        "has_weight",
        "active",
	];

	protected $hidden = [];

    protected $guarded = [];

    protected $casts = [
        "has_dimension" => "boolean",
        "has_weight" => "boolean",
        "active" => "boolean",
    ];

    // TODO - remove / comment when done developing
    // protected function onInitializedFromNancy(){
    //     $this->has_dimension = false;
    //     $this->has_weight = false;
    // }
}