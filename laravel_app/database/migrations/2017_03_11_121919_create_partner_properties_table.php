<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnerPropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('partner_properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('partner_id', 36);
            $table->text('properties');
            $table->string('lostpassword_token', 100)->index();
            $table->integer('lostpassword_created_at')->index();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('partner_properties');
    }
}
