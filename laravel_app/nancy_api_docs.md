# AKTIVNI URL
http://85.217.170.209:10002

# dostopi
URI       : http://85.217.170.209:10002/Nancy/API/{type}/{method}
username  : ...
password  : ...
type      : ime tipa, npr. XGeDržava
method    : ime metode, npr. NancyDbCount
parametri : opisani spodaj

# info
- Vse klice lahko razdelimo na vsebinske sklope:
    A. COUNT
    B. LIST
    C. UPDATE
    Č. INSERT
    D. DELETE
    E. razno
- vsi parametri se sedaj podajajo opcijsko, razen zgornjih dveh {type} in {method}
- povsem vseeno mi je, ali kličeš GET ali POST, delati bi moralo oboje
- 

# Primer klica
http://85.217.170.209:10002/Nancy/API/XGeNaslovnik/NancyDbList?freeTextSearch=Helmut

# COUNT
API - COUNT [GET ali POST]
==========================
sintaksa  : http://85.217.170.209:10002/Nancy/API/{type}/NancyDbCount
parametri : criteria=
            freeTextCriteria=

rezultat  : število zapisov
podprto   : XSkPartner, XSkPartnerKontakt, XGeDržava, XGeNaslovnik, XGeNaročilo, XGeNaročiloPaket

primeri   : http://85.217.170.209:10002/Nancy/API/XSkPartner/NancyDbCount
            http://85.217.170.209:10002/Nancy/API/XSkPartnerKontakt/NancyDbCount?criteria=PartnerId='cea70c04-cee3-e611-989f-902b3435e7c8'
            http://85.217.170.209:10002/Nancy/API/XGeDržava/NancyDbCount?freeTextCriteria=otoki
            http://85.217.170.209:10002/Nancy/API/XGeNaslovnik/NancyDbCount?criteria=PartnerId='cea70c04-cee3-e611-989f-902b3435e7c8'&freeTextCriteria=Berlin
            http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyDbCount?criteria=PartnerId='cea70c04-cee3-e611-989f-902b3435e7c8'
            http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyDbCount?criteria=PartnerId='cea70c04-cee3-e611-989f-902b3435e7c8'&freeTextCriteria=Dortmund
            http://85.217.170.209:10002/Nancy/API/XGeNaročiloPaket/NancyDbCount?criteria=NaročiloId='0b58d0de-c9e4-e611-81fc-3052cb29a8ca'



# LIST
API - LIST [GET ali POST]
=========================
sintaksa  : http://85.217.170.209:10002/Nancy/API/{type}/NancyDbList
parametri : criteria=
            freeTextCriteria=
            properties=, naštejejo se želena polja, če je prazno, se vrne privzeti spisek
            sort=, naštetejo se želena polja, če je prazno, se upošteva ID
            skip=, število zapisov, ki se preskočijo, če je prazno, se upošteva 0
            take=, število zapisov, ki se vrnejo če je prazno, se upošteva 25; maksimum je 1000

rezultat  : JSON zapisi
podprto   : XSkPartner, XSkPartnerKontakt, XGeDržava, XGeNaslovnik, XGeNaročilo, XGeNaročiloPaket

primeri   : http://85.217.170.209:10002/Nancy/API/XSkPartner/NancyDbList?sort=Ime&skip=3&take=2
            http://85.217.170.209:10002/Nancy/API/XSkPartnerKontakt/NancyDbList?criteria=PartnerId='cea70c04-cee3-e611-989f-902b3435e7c8'&sort=Oznaka
            http://85.217.170.209:10002/Nancy/API/XGeDržava/NancyDbList?freeTextCriteria=otoki
            http://85.217.170.209:10002/Nancy/API/XGeNaslovnik/NancyDbList?criteria=PartnerId='cea70c04-cee3-e611-989f-902b3435e7c8'&freeTextCriteria=Berlin
            http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyDbList?criteria=PartnerId='cea70c04-cee3-e611-989f-902b3435e7c8'&sort=Oznaka descending
            http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyDbList?criteria=PartnerId='cea70c04-cee3-e611-989f-902b3435e7c8'&freeTextCriteria=Dortmund
            http://85.217.170.209:10002/Nancy/API/XGeNaročiloPaket/NancyDbList?criteria=NaročiloId='0b58d0de-c9e4-e611-81fc-3052cb29a8ca'&sort=ZaporednaŠtevilka  
            http://85.217.170.209:10002/Nancy/API/XGeNaročiloSledenje/NancyDbList?id=21afbcf5-cce4-e611-81fc-3052cb29a8ca
            http://85.217.170.209:10002/Nancy/API/XGeVrstaPošiljke/NancyDbList

# UPDATE
API - UPDATE [GET ali POST]
===========================
sintaksa  : http://85.217.170.209:10002/Nancy/API/{type}/NancyDbUpdate
parametri : id=, GUID zapisa, ki se posodablja, podatek je obvezen
            {polje_1}={vrednost_1}
            {polje_2}={vrednost_2}
            ...
            {polje_N}={vrednost_N}
              - Naštejejo se (v obliki key=value) nove vrednosti polj, ki se posodabljajo.
              - Dovoljeno je spreminjati samo določena polja, v nasprotnem primeru program vrne napako.
              - Katera polja je dovoljeno spreminjati, je navedeno spodaj, pri posameznih tabelah.

rezultat  : true, false (ali exception)
podprto   : XGeNaslovnik, XGeNaročilo, XGeNaročiloPaket

primeri   : http://85.217.170.209:10002/Nancy/API/XGeNaslovnik/NancyDbUpdate?id=1367b4bc-22e7-e611-81fc-3052cb29a8ca&Naslovnik_2=Čevapčič&Kraj=Leskovac
              - Popravi se zapis v tabeli naslovnikov tako, da se vpiše v "Naslovnik_2" vrednost "Čevapčič" in v "Kraj" vrednost "Leskovac"
            
            http://85.217.170.209:10002/Nancy/API/XGeNaslovnik/NancyDbUpdate?id=1367b4bc-22e7-e611-81fc-3052cb29a8ca&Naslovnik_2=
              - Ponovno se popravi zapis, "Kraj" ostane "Leskovac", polje "Naslovnik_2" pa se anulira

            http://85.217.170.209:10002/Nancy/API/XGeNaslovnik/NancyDbUpdate?id=1367b4bc-22e7-e611-81fc-3052cb29a8ca&DržavaId=185fed38-cae3-e611-989f-902b3435e7c8
              - Država se spremeni na "Zelenortske otoke" (kjerkoli to že je)
              
              - Dovoljena polja za spreminjanje so:
                  - DržavaId 
                  - Naslovnik_1
                  - Naslovnik_2
                  - Naslov_1
                  - Naslov_2
                  - Pošta
                  - Kraj
                  - Ime
                  - Telefon
                  - Aktivno

primeri   : http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyDbUpdate?id=e0aebcf5-cce4-e611-81fc-3052cb29a8ca&Kraj=London
              - Popravi se zapis v tabeli naslovnikov tako, da se vpiše "Kraj" je "London"
            
              - Dovoljena polja za spreminjanje so:

                  - DržavaId 
                  - Naslovnik_1
                  - Naslovnik_2
                  - Naslov_1
                  - Naslov_2
                  - Pošta
                  - Kraj
                  - Ime
                  - Telefon

              - Če status naročila ne dovoljuje spreminjanja, bo zahtevek zavrnjen (result = false).

primeri   : http://85.217.170.209:10002/Nancy/API/XGeNaročiloPaket/NancyDbUpdate?id=edde375c-d5e4-e611-81fc-3052cb29a8ca&Teža=42.0
              - Popravi se teža paketa na 42kg.
            
              - Dovoljena polja za spreminjanje so:

                  - Teža
                  - Dolžina
                  - Širina
                  - Višina

              - Če status naročila ne dovoljuje spreminjanja, bo zahtevek zavrnjen (result = false).


# INSERT
v nekaterih primerih pa želiš dodajati podatke, kar je relativno podoben postopek kot posodabljanje
razlika je v tem, da so dodatno obvezna določena polja, nazaj pa dobiš ID novo dodanega zapisa, če je vse v redu
edini tabeli, kjer dodajaš zapise sta pravzaprav samo Naročilo in NaročiloPaket
primer:

API - INSERT [GET ali POST]
===========================

sintaksa  : http://85.217.170.209:10002/Nancy/API/{type}/NancyDbInsert
parametri : {polje_1}={vrednost_1}
            {polje_2}={vrednost_2}
            ...
            {polje_N}={vrednost_N}
              - Naštejejo se (v obliki key=value) nove vrednosti polj, ki se dodajajo.
              - Dovoljeno je spreminjati samo določena polja, v nasprotnem primeru program vrne napako.
              - Katera polja je dovoljeno spreminjati, je navedeno zgoraj, pri posodabljanje.
              - Nekatera polja so obvezna, to je navedeno spodaj.

rezultat  : GUID dodanega zapisa, ali NULL pri zavrnitvi (ali exception)
podprto   : XGeNaročilo, XGeNaročiloPaket

primeri   : http://localhost:10002/Nancy/API/XGeNaročilo/NancyDbInsert?partnerKontaktId=5862a79d-cfe3-e611-989f-902b3435e7c8&DržavaId=185fed38-cae3-e611-989f-902b3435e7c8&Naslovnik_1=Batman
              - Naredi novo naročilo v pripravi, za danega naročnika, za državo vpiše "Zelenortske otoke" (še vedno ne vem, kje) in naslovi na "Batmana"
              - Nazaj se vrne GUID novega zapisa, ki se potem lahko ureja, recimo: 215267b1-21ee-e611-81fc-3052cb29a8ca

primeri   : http://localhost:10002/Nancy/API/XGeNaročiloPaket/NancyDbInsert?naročiloId=215267b1-21ee-e611-81fc-3052cb29a8ca&Dolžina=200&Teža=3.14
              - Naročilu v zgornjemu primeru doda nov paket, z dolžino 200cm in težo 3,14kg

primeri   : http://localhost:10002/Nancy/API/XGeNaročiloPaket/NancyDbInsert?naročiloId=215267b1-21ee-e611-81fc-3052cb29a8ca&Širina=200
              - Naročilu v zgornjemu primeru doda še en nov paket, s širino 200cm

# DELETE - paketi
izjemoma lahko pride do brisanja, pravzaprav samo pri podatkih o paketih
recimo da nekdo vpiše paket preveč, pa ga moraš zbrisati

API - DELETE [GET ali POST]
===========================

sintaksa  : http://85.217.170.209:10002/Nancy/API/{type}/NancyDbDelete
parametri : id=
              - Poda se ID zapisa, ki se ga briše
              - Če status ne dovoljuje brisanja bo zahtevek zavrnjen (result = false).

rezultat  : true za uspeh, false za neuspeh (ali exception)
podprto   : XGeNaročiloPaket

primeri   : http://localhost:10002/Nancy/API/XGeNaročiloPaket/NancyDbDelete?id=52ecd9ba-28ee-e611-81fc-3052cb29a8ca
              - Izbriše paket



# RAZNO
ostane še zadnji sklop, morda najpomembnejši

API - RAZNO [GET ali POST]
==========================

E1. ti vrne PDF s tovornim listom
sintaksa  : http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyPdf
parametri : id=
rezultat  : vrne PDF za naročilo z id
primer    : http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyPdf?id=0b58d0de-c9e4-e611-81fc-3052cb29a8ca

E2. ti vrne spisek možnih servisov, za izbiro stranke
sintaksa  : http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyIzračunSpisek
parametri : id=
rezultat  : vrne izračune (cenike) za naročilo z id
primer    : http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyIzračunSpisek?id=0b58d0de-c9e4-e611-81fc-3052cb29a8ca

E3. je v delu, tega moram še potestirati
sintaksa  : http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyIzračunIzberi
parametri : id=
            servisId= [obvezno]
            cenikId= [obvezno]
            linijaId= [obvezno]            
rezultat  : zaključi in odda naročilo z izbranim servisem


# pregled in komentarji 17.2.2017

## Address / XGeNaslovnik
- manjka polje za *email*, poprej imenovano "kontaktEpošta" (če je to polje potrebno)

## Country / XGeDržava
- vse ok

## Delivery / XGeNaročilo
- manjka polje *status*, poprej polje "statusVrednost"
- manjka polje *status_code*, poprej polje "statusOznaka"
- manjka polje *type*, poprej polje "vrstaPošiljke"
- manjka polje *service_code*, poprej polje "storitevOznaka"
- manjka polje *email*, poprej polje "kontaktEpošta" (če je to polje potrebno)
- manjka polje *comment*, poprej polje "komentar"
- manjka polje *additional_assurance*, poprej polje "dodatnoZavarovanje" (če je to polje potrebno v tem hipu)
- manjka polje *additional_assurance_package_value*, poprej polje "dodatnoZavarovanjeVrednostBlaga" (če je to polje potrebno v tem hipu)
- manjka polje *additional_assurance_package_description*, poprej polje "dodatnoZavarovanjeOpisBlaga" (če je to polje potrebno v tem hipu)

## PartnerKontakt / XSkPartnerKontakt
- manjka polje *email*, poprej polje "epošta"


# review in komentarji 19.2.2017

## PartnerKontakt / XSkPartnerKontakt
- možnost posodabljanja polj "epošta" in "geslo"
- pri partner kontaktu z oznako "GR-001" je geslo NULL. Je tak zapis v bazi ali ti namenoma ne vračaš tega podatka?

## XGeNaročilo
- insert mi ne vrača ničesar, enako je če uporabim tvoje primere klicev

## sorting ne deluje
- če za sort uporabim "ime+" ne sortira, če uporabim "ime-", vrže napako
  **ODGOVOR** : uporabi ime polja za ASC "ime", za padajoče pa "ime descending"

## XGeNaslovnik
- ne deluje posodobitev polja "aktivno", če posredujem boolean



# review in komentarji 20.2.2017, 21.2.2017

## PackageType / XGeVrstaPošiljke
- trenutni polji *imaTežo* in  *imaDimenzije*, če nista namenjeni spletu, bi prosil za podvojitev
  teh dveh polj v smislu *webImaTežo* in *webImaDimenzije*, da lahko GEX kasneje sam nadzira stvari.

## DeliveryPackage / XGeNaročiloPaket
- INFORMACIJAJSKEGA ZNAČAJA: v primeru, da tip paketa ne potrebuje teže in dimenzij, uporabnik vpiše le število paketov, v bazi
  pa jaz ustvarjam prazne zapise, ki na koncu povedo koliko je bilo paketov.
  V primeru drugačne rešitve ali drugega polja, prosim javite (Piki ali GEX)

## DeliveryService / API klic "/API/XGeNaročilo/NancyIzračunSpisek"
- polje "şervisId" predlagam, da se zamenja za "servisId". Glej prvo črko, ki ni "s" :)
- kaj je polje "uspeh" ?
- prazne informacije za "linijaId", "cenikId", "cena", "predvidenČas". Ne vem, če je to vezano na napačne podatke naslovnika ali kaj čisto tretjega.


# review in komentarji 23.2.2017

## Delivery / XGeNaročilo
- ko enkrat izberem tip storitve, ne morem več shranjevati. Sem narobe razumel, da lahko stranka ureja vse kar je pred statusom 20?

## RAZNO
### E1: klic za PDF
- vrača "Exception has been thrown by the target of an invocation"
  http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyPdf?id=4ee9ebd2-d0f6-e611-80c3-4ecd4964f4b4
  http://85.217.170.209:10002/Nancy/API/XGeNaročilo/NancyPdf?id=28e5a57c-cce4-e611-81fc-3052cb29a8ca
