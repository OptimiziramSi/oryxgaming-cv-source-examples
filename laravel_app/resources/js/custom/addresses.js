/*************************************************
		ADDRESSES - START
*************************************************/

function ad_init( ajax_url, ajax_update_url, delivery_from_address_url ){
	window.AdVue = new Vue({
		el: ad_selector,
		data: {
			'ajax_url' : ajax_url,
			'ajax_update_url' : ajax_update_url,
			'delivery_from_address_url': delivery_from_address_url,

			'search' : '',
			'active' : '',

			'loading' : false,
			'page' : 1,
			'pages' : 1,
			'sort_by': 'addressee_1',
			'sort_type': 'asc',

			'addresses' : [],
			'search_timeout': null,
			'edit' : {
				'saving': false,
				'address' : {},
			},
		},
		methods: {
			loadAddresses: function(){
				this.loading = true;
				$.get(
					this.ajax_url,
					{
						search: this.search,
						active: this.active,
						sort_by: this.sort_by,
						sort_type: this.sort_type,
						timespan: this.timespan,
						page: this.page,
					},
					this.onLoadAddresses,
					'json'
				).error(this.onLoadAddressesError);
			},
			onLoadAddresses: function(data){
				var compare_keys = ['search','active','page','sort_by', 'sort_type'];
				var response_request_ok = true;
				for (var i = 0; i < compare_keys.length; i++) {
					var key = compare_keys[i];
					if(data[key] != this[key]){
						response_request_ok = false;
						break;
					}
				}

				if( ! response_request_ok ){
					return;
				}

				this.loading = false;
				this.pages = data.pages;
				this.addresses = data.addresses;

				guide('addresses.search');
				$(document).on('guide.addresses.search.closed', function(){
					guide('addresses.filter');
				});

				setTimeout(function(){
					tooltip_init();
				}, 100);

				// this.edit_address(data.addresses[0]);
			},
			onLoadAddressesError: function(xhr, ajaxOptions, thrownError){
				// TODO
				console.log("onLoadAddressesError", xhr, ajaxOptions, thrownError);
			},
			prev_page: function(){
				this.open_page( this.page - 1 );
				return false;
			},
			next_page: function(){
				this.open_page( this.page + 1 );
				return false;
			},
			open_page: function( page ){
				if( page < 1 ){
					page = 1;
				}

				if( page > this.pages ){
					page = this.pages;
				}

				if( this.page == page ){
					return;
				}

				this.page = page;

				this.loadAddresses();
			},
			edit_address: function( address ){
				this.edit.address = jQuery.extend(true, {}, address);

				$('#edit_address_modal').modal('open');
				$('#edit_address_modal .modal-content').scrollTop(0);

				var self = this;
				setTimeout( function(){
					if(Materialize.updateTextFields){
						Materialize.updateTextFields();
					}

					$("select#edit_address_country").find("option").prop('selected', false);
					$("select#edit_address_country").find("option[value='"+self.edit.address.country_id+"']").prop('selected', true);

					$("select#edit_address_country").material_select();
				}, 20 );
			},
			save_address: function(){
				this.edit.saving = true;

				$.post(
					this.ajax_update_url + '/' + this.edit.address.id,
					{
						_token: token(),
						address: this.edit.address,
					},
					this.on_save_address,
					'json'
				).error(this.on_save_address_error);
				
			},
			on_save_address: function(data){
				this.edit.saving = false;

				if( data.error ){
					alert(data.error);
					return;
				}

				for (var ad_idx = 0; ad_idx < this.addresses.length; ad_idx++) {
					if( this.addresses[ad_idx].id == this.edit.address.id ){
						this.addresses[ad_idx] = jQuery.extend(true, {}, this.edit.address);
						break;
					}
				}

				$('#edit_address_modal').modal('close');
			},
			on_save_address_error: function(xhr, ajaxOptions, thrownError){
				alert("Pri posodabljanju naslovnika je prišlo do napake. Prosimo poskusite kasneje.");
				this.edit.saving = false;
			},
			cancel_edit_address: function(){
				$('#edit_address_modal').modal('close');
			},
			reset_search: function(){
				this.search='';
				$("label[for='search_address']").removeClass('active');
			},
			send_package: function(address){
				var location = this.delivery_from_address_url;
				console.log(location);
				window.location = location.split('{address_id}').join(address.id);
			},
			sort: function( by_field ){
				if( this.sort_by != by_field ){
					this.sort_by = by_field;
					this.sort_type = 'asc';
				} else {
					this.sort_type = ('asc' == this.sort_type) ? 'desc' : 'asc';
				}

				this.loadAddresses();
			},
		},
		watch: {
			search: function(){
				clearTimeout( this.search_timeout );
				this.loading = true;

				this.search_timeout = setTimeout(this.loadAddresses, 0.6 * 1000);
			},
			active: function(){
				this.page = 1;
				this.loadAddresses();
			}
		},
		mounted: function(){
			var self = this;
			this.$on('select_active_changed', function(val){
				self.active = val;
			});
			this.$on('edit_address_country_changed', function(val, text){
				self.edit.address.country_id = val;
				self.edit.address.country_display_name = text;
			});
			this.loadAddresses();

			$(ad_selector).show();
		},
	});
}

/*************************************************
		ADDRESSES - END
*************************************************/