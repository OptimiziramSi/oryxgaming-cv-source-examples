/*************************************************
		DELIVERIES - START
*************************************************/

function de_init( ajax_url, edit_url, pdf_url_base, pdf_download_url_base, tracking_url_base ){
	window.DeVue = new Vue({
		el: de_selector,
		data: {
			'ajax_url' : ajax_url,
			'edit_url' : edit_url,

			'search' : '',
			'period': '',
			'status': '',
			'partner': '',

			'sort_by': 'updated',
			'sort_type': 'desc',

			'loaded' : false,
			'loading' : false,
			'request_id' : null,
			'page' : 1,
			'pages' : 1,
			'deliveries' : [],
			'search_timeout': null,
			'edit' : {
				'saving': false,
				'search': '',
				'delivery' : {},
			},

			'pdf_url_base': pdf_url_base,
			'pdf_download_url_base': pdf_download_url_base,
			'pdf_loading': false,
			'pdf_url': '',
			'pdf_download_url': '',

			'tracking_url_base': tracking_url_base,
			'tracking_loading': false,
			'tracking_delivery_id': '',
			'tracking_list': [],
			'tracking_urls': [],
			'tracking_courier_display_name': null,
			'tracking_awb': null,
		},
		methods: {
			loadDeliveriesTrigger:function(){
				clearTimeout( this.search_timeout );
				this.loading = true;

				this.search_timeout = setTimeout(this.loadDeliveries, 0.6 * 1000);
			},
			loadDeliveries: function(){
				this.loading = true;

				$.get(
					this.ajax_url,
					{
						search: this.search,
						period: this.period,
						status: this.status,
						partner: this.partner,
						sort_by: this.sort_by,
						sort_type: this.sort_type,
						page: this.page,
					},
					this.onLoadDeliveries,
					'json'
				);
			},
			onLoadDeliveries: function(data){
				var compare_keys = ['search','page','period','status','partner','sort_by', 'sort_type'];
				var response_request_ok = true;
				for (var i = 0; i < compare_keys.length; i++) {
					var key = compare_keys[i];
					if(data[key] != this[key]){
						response_request_ok = false;
						break;
					}
				}

				if( ! response_request_ok ){
					return;
				}

				this.loading = false;
				this.pages = data.pages;
				this.deliveries = data.deliveries;

				setTimeout(function(){
					tooltip_init();
				}, 100);


				guide('deliveries.search', 300);
				$(document).on('guide.deliveries.search.closed', function(){
					guide('deliveries.filter1');
				});
				$(document).on('guide.deliveries.filter1.closed', function(){
					guide('deliveries.filter2');
				});

				// this.edit_delivery(this.deliveries[2]);
			},
			prev_page: function(){
				this.open_page( this.page - 1 );
				return false;
			},
			next_page: function(){
				this.open_page( this.page + 1 );
				return false;
			},
			open_page: function( page ){
				if( page < 1 ){
					page = 1;
				}

				if( page > this.pages ){
					page = this.pages;
				}

				if( this.page == page ){
					return;
				}

				this.page = page;

				this.loadDeliveries();
			},
			save_delivery: function(){
				this.edit.saving = true;

				for (var i = this.edit.delivery.paketi.length - 1; i >= 0; i--) {
					var paket = this.edit.delivery.paketi[i];
					if( '' == paket.teza && '' == paket.dolzina && '' == paket.sirina && '' == paket.visina ){
						this.edit.delivery.paketi.splice(i, 1);
					}
				}

				$.post(
					this.ajax_update_url + '/' + this.edit.delivery.id,
					{
						_token: token(),
						delivery: this.edit.delivery,
					},
					this.on_save_delivery,
					'json'
				);
				
			},
			on_save_delivery: function(data){
				this.check_packages();
				this.edit.saving = false;

				if( data.error ){
					alert(data.error);
					return;
				}

				for (var ad_idx = 0; ad_idx < this.deliveries.length; ad_idx++) {
					if( this.deliveries[ad_idx].id == this.edit.delivery.id ){
						this.deliveries[ad_idx] = jQuery.extend(true, {}, this.edit.delivery);
						break;
					}
				}

				$('#edit_delivery_modal').modal('close');
			},
			reset_search: function(){
				this.search='';
				$("label[for='search_delivery']").removeClass('active');
			},
			print: function( delivery_id ){
				
				this.pdf_loading = true;
				this.pdf_url = this.pdf_url_base.replace('REPLACE_ME_WITH_DELIVERY_ID', delivery_id);
				this.pdf_download_url = this.pdf_download_url_base.replace('REPLACE_ME_WITH_DELIVERY_ID', delivery_id);

				// PDFObject.embed('http://gex-prizma2.localhost/deliveries/6826304b-c61d-e711-80c6-4ecd4964f4b4/pdf', "#pdf_viewer");

				$('#pdf_modal').modal('open');
				$('#pdf_modal .modal-content').scrollTop(0);

			},
			cancel_pdf_model: function(){
				$('#pdf_modal').modal('close');
			},
			track: function( delivery_id ){
				this.tracking_loading = true;
				this.tracking_delivery_id = delivery_id;
				
				$.get(
					this.tracking_url_base.replace('REPLACE_ME_WITH_DELIVERY_ID', delivery_id),
					{},
					this.on_track,
					'json'
					);
				// TODO - add ajax error handling

				$('#track_modal').modal('open');
			},
			on_track: function( data ){
				if( ! data.id || data.id != this.tracking_delivery_id){
					return;
				}

				if( data.error ){
					alert(data.error);
					return;
				}

				this.tracking_list = data.trackings;
				this.tracking_urls = data.tracking_urls;
				this.tracking_courier_display_name = data.tracking_courier_display_name;
				this.tracking_awb = data.tracking_awb;
				this.tracking_loading = false;
			},
			close_track: function(){
				this.tracking_delivery_id = '';
				$('#track_modal').modal('close');
			},
			sort: function( by_field ){
				if( this.sort_by != by_field ){
					this.sort_by = by_field;
					this.sort_type = 'asc';
				} else {
					this.sort_type = ('asc' == this.sort_type) ? 'desc' : 'asc';
				}

				this.loadDeliveries();
			},
		},
		watch: {
			search: function(){
				this.loadDeliveriesTrigger();
			},
			period: function(){
				this.loadDeliveriesTrigger();
			},
			status: function(){
				this.loadDeliveriesTrigger();
			},
			partner: function(){
				this.loadDeliveriesTrigger();
			},
		},
		mounted: function(){
			var self = this;
			$(document).on('change', '#deliveries_status_select', function(){
				self.status = $(this).val() ? $(this).val().join(',') : '';
			});
			$(document).on('change', '#deliveries_period_select', function(){
				self.period = $(this).val();
			});
			$(document).on('change', '#deliveries_partner_contact_filter', function(){
				self.partner = $(this).val() ? $(this).val().join(',') : '';
			});
			$("#deliveries_partner_contact_filter").trigger('change');

			this.loadDeliveries();

			$(de_selector).show();
		},
	});
}

/*************************************************
		DELIVERIES - END
*************************************************/