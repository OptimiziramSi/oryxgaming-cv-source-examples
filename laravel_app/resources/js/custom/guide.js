	/*************************************************
			GUIDE - START
	*************************************************/

	var guides = {
		'delivery.search_addressee': 'Naslovnike lahko iščete hitreje.',
		'delivery.in_history': 'Oddano pošiljko si lahko ogledate v zgodovini pošiljk.',
		'delivery.can_download_pdf': 'PDF si lahko tudi prenesete.',

		'deliveries.search': 'Iščite po pošiljkah.',
		'deliveries.filter1': 'Filtrirajte po pošiljkah.',
		// 'deliveries.filter2': 'Ali po statusu pošiljke.',
		// 'deliveries.filter2': 'Ali po statusu pošiljke.',

		'addresses.search': 'Hitri iskalnik po naslovnikih.',
		'addresses.filter': 'Filtrirajte naslovnike.',

		'help.add_item': 'Želite dodati vsebino?',
		'help.only_admin_see': 'Izbira za prikaz osnutkov, osnutke, neobjavljene vsebine ter gumbe za urejanje in izbris, vidite samo vi / Global Express.',

		'warning': '',
	};
	var guides_params = {};
	var guide_box_id = 1;
	var guide_viewed_url = '';
	var guide_viewed = [];
	var guide_is_init = false;
	var guide_check_viewed = true;
	var guide_hide_open = false;

	function guide_init(){
		$(document).on('click', '.guide-box', function(e){
			e.preventDefault();
			guide_close($(this).attr('data-guide-code'));
		});

		var body_data = $('body').data();
		if(body_data.guideViewedUrl){
			guide_viewed_url = body_data.guideViewedUrl;
			guide_viewed = body_data.guideViewed;

			$(document).on('closeModal', '.modal', guide_reopen_hidden);
			$(document).on('openModal', '.modal', guide_hide_opened);

			guide_is_init = true;
		}
	}
	function guide(code, delay){
		if(!guide_is_init)
			return;

		if(guide_hide_open)
			return;

		if( guide_check_viewed && -1 != guide_viewed.indexOf(code)){
			var trigger_code = 'guide.' + code + '.closed';
			$(document).trigger(trigger_code);
			return;
		}

		var content = guides.hasOwnProperty(code) ? guides[code] : false;
		if(!content)
			return;

		var guide_obj = false;
		if(guides_params.hasOwnProperty(code)){
			guide_obj = guides_params[code];
		}

		if(!guides_params.hasOwnProperty(code))
			guides_params[code] = {shown: false, showing: false, closed: false, closing: false, hidden: false};

		guide_obj = guides_params[code];

		if(guide_obj.shown || guide_obj.showing)
			return;

		var $el = $("[data-guide='"+code+"']:visible");
		if(0 == $el.length)
			return;

		guide_obj.showing = true;

		setTimeout(function(){
			guide_generate(code, $el, content);
		}, delay ? delay : 0);
	}

	function warning($elements, content){
		$elements.filter(':visible').each(function(i,e){
			guide_show('warning', content, $(e), 'top');			
		});	
	}

	function guide_generate(code, $el, content){
		var guide_obj = guides_params[code];
		setTimeout(function(){ guide_obj.shown = true; guide_obj.showing = false; guide_obj.closed = false; guide_obj.closing = false; }, 400);

		$el.each(function(i,e){
			guide_show(code, content, $(e), $(e).attr('data-guide-position'));			
		});	
	}

	function guide_show(code, content, origin, position){
		var tooltipPosition = position;
		var tooltipEl = guide_render_box(code, content, position);

		var margin = 5;

		tooltipEl.velocity('stop');
		backdrop.velocity('stop');
		tooltipEl.css({ display: 'block', left: '0px', top: '0px' });

		// Tooltip positioning
		var originWidth = origin.outerWidth();
		var originHeight = origin.outerHeight();

		var tooltipHeight = tooltipEl.outerHeight();
		var tooltipWidth = tooltipEl.outerWidth();
		var tooltipVerticalMovement = '0px';
		var tooltipHorizontalMovement = '0px';
		var scaleXFactor = 8;
		var scaleYFactor = 8;
		var targetTop, targetLeft, newCoordinates;

		if (tooltipPosition === "top") {
			// Top Position
			targetTop = origin.offset().top - tooltipHeight - margin;
			targetLeft = origin.offset().left + originWidth/2 - tooltipWidth/2;
			newCoordinates = guide_reposition_within_screen(targetLeft, targetTop, tooltipWidth, tooltipHeight);

			tooltipVerticalMovement = '-10px';
			backdrop.css({
			bottom: 0,
			left: 0,
			borderRadius: '14px 14px 0 0',
			transformOrigin: '50% 100%',
			marginTop: tooltipHeight,
			marginLeft: (tooltipWidth/2) - (backdrop.width()/2)
			});
		}
		// Left Position
		else if (tooltipPosition === "left") {
			targetTop = origin.offset().top + originHeight/2 - tooltipHeight/2;
			targetLeft =  origin.offset().left - tooltipWidth - margin;
			newCoordinates = guide_reposition_within_screen(targetLeft, targetTop, tooltipWidth, tooltipHeight);

			tooltipHorizontalMovement = '-10px';
			backdrop.css({
			top: '-7px',
			right: 0,
			width: '14px',
			height: '14px',
			borderRadius: '14px 0 0 14px',
			transformOrigin: '95% 50%',
			marginTop: tooltipHeight/2,
			marginLeft: tooltipWidth
			});
		}
		// Right Position
		else if (tooltipPosition === "right") {
			targetTop = origin.offset().top + originHeight/2 - tooltipHeight/2;
			targetLeft = origin.offset().left + originWidth + margin;
			newCoordinates = guide_reposition_within_screen(targetLeft, targetTop, tooltipWidth, tooltipHeight);

			tooltipHorizontalMovement = '+10px';
			backdrop.css({
			top: '-7px',
			left: 0,
			width: '14px',
			height: '14px',
			borderRadius: '0 14px 14px 0',
			transformOrigin: '5% 50%',
			marginTop: tooltipHeight/2,
			marginLeft: '0px'
			});
		}
		else {
			// Bottom Position
			targetTop = origin.offset().top + origin.outerHeight() + margin;
			targetLeft = origin.offset().left + originWidth/2 - tooltipWidth/2;
			newCoordinates = guide_reposition_within_screen(targetLeft, targetTop, tooltipWidth, tooltipHeight);
			tooltipVerticalMovement = '+10px';
			backdrop.css({
			top: 0,
			left: 0,
			marginLeft: (tooltipWidth/2) - (backdrop.width()/2)
			});
		}

		// Set tooptip css placement
		tooltipEl.css({
			top: newCoordinates.y,
			left: newCoordinates.x
		});
		tooltipEl.find('.arrow-holder').css('left', tooltipWidth/2 + targetLeft - newCoordinates.x);

		// Calculate Scale to fill
		scaleXFactor = Math.SQRT2 * tooltipWidth / parseInt(backdrop.css('width'));
		scaleYFactor = Math.SQRT2 * tooltipHeight / parseInt(backdrop.css('height'));

		tooltipEl.velocity({ marginTop: tooltipVerticalMovement, marginLeft: tooltipHorizontalMovement}, { duration: 120, queue: false })
			.velocity({opacity: 1}, {duration: 150, delay: 50, queue: false});
		backdrop.css({ display: 'block' })
			.velocity({opacity:1},{duration: 55, delay: 0, queue: false})
			.velocity({scaleX: scaleXFactor, scaleY: scaleYFactor}, {duration: 300, delay: 0, queue: false, easing: 'easeInOutQuad'});
	}

	function guide_render_box(code, content, position){
		var tooltip = $('<div class="guide-box"></div>');

		if('warning' == code){
			tooltip.addClass('warning');
		}

		tooltipOk = $('<span class="ok"><a href="#ok" class="btn red">OK</a></span>');
		// tooltipIcon = $('<span class="icon"><i class="material-icons">info</i></span>');
		tooltipText = $('<span class="text"></span>').html(content);

		// Create tooltip
		tooltip
		.append(tooltipOk)
		// .append(tooltipIcon)
		.append(tooltipText)
		.appendTo($('body'))
		.attr('id', 'guide-' + guide_box_id)
		.addClass('z-depth-4')
		.addClass('guide-' + code)
		.attr('data-guide-code', code)
		;
		guide_box_id++;

		var $arrow = $('<span class="arrow-holder"></span>')
			.append($('<i class="material-icons bottom-arrow">arrow_drop_down</i>'))
			.append($('<i class="material-icons top-arrow">arrow_drop_up</i>'))
		;
		$arrow.appendTo(tooltip);

		if('top' == position){
			$arrow.addClass('bottom-arrow');
		}
		else{
			$arrow.addClass('top-arrow');
		}
		
		if('top' == position){
			tooltip.addClass('bottom-arrow');
		}
		else{
			tooltip.addClass('top-arrow');
		}

		// Create backdrop
		backdrop = $('<div class="backdrop"></div>');
		backdrop.appendTo(tooltip);
		return tooltip;
	}

	function guide_reposition_within_screen(x, y, width, height) {
		var newX = x;
		var newY = y;

		if (newX < 0) {
		newX = 4;
		} else if (newX + width > window.innerWidth) {
		newX -= newX + width - window.innerWidth;
		}

		if (newY < 0) {
		newY = 4;
		} else if (newY + height > window.innerHeight + $(window).scrollTop) {
		newY -= newY + height - window.innerHeight;
		}

		return {x: newX, y: newY};
	};

	function guide_close(code){
		if('warning' != code){
			var guide_obj = guides_params[code];
			if(guide_obj.closing || guide_obj.closed)
				return;

			if(!guide_hide_open)
				guide_set_viewed(code);

			guide_obj.closing = true;
			
			if(guide_hide_open)
				guide_obj.hidden = true;
		}

		setTimeout(function(){
			if('warning' != code){
				guide_obj.shown = false;
				guide_obj.showing = false;
				guide_obj.closed = true;
				guide_obj.closing = false;
			}

			if(!guide_hide_open){
				var trigger_code = 'guide.' + code + '.closed';
				delete guides_params[code];
				$(document).trigger(trigger_code);
			}
		}, 120);

		var tooltipEl = $("[data-guide-code='"+code+"']");
		var backdrop = tooltipEl.find('.backdrop');

		setTimeout(function() {
		    tooltipEl.velocity({
		      opacity: 0, marginTop: 0, marginLeft: 0}, { duration: 225, queue: false});
		    backdrop.velocity({opacity: 0, scaleX: 1, scaleY: 1}, {
		      duration:225,
		      queue: false,
		      complete: function(){
		        backdrop.css('display', 'none');
		        tooltipEl.css('display', 'none');
		        tooltipEl.remove();
		      }
		    });
		},20);
	}

	function guide_set_viewed(code){
		guide_viewed.push(code);
		$.post(
			guide_viewed_url,
			{
				action: 'guide_viewed',
				guide_code: code,
				_token: token(),
			},
			function(data){

			},
			'json'
		);
	}

	function guide_hide_opened(){
		clearTimeout(guide_reopen_hidden_timeout);
		if(guide_hide_open){
			return;
		}

		guide_hide_open = true;

		for(var code in guides_params){
			if(!guides_params[code].hidden){
				guide_close(code);
			}
		}
	}

	var guide_reopen_hidden_timeout;
	function guide_reopen_hidden(){
		clearTimeout(guide_reopen_hidden_timeout);
		if(guide_hide_open){

			guide_reopen_hidden_timeout = setTimeout(function(){
				guide_hide_open = false;
				for(var code in guides_params){
					if(guides_params[code].hidden){
						guides_params[code].hidden = false;
						guide(code);
					}
				}
			}, 300);

		}
	}

	guide_init();

	/*************************************************
			GUIDE - END
	*************************************************/