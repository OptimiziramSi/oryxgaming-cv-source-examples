/*************************************************
		HELP - START
*************************************************/

function h_init( data ){
	window.HVue = new Vue({
		el: he_selector,
		data: {
			'ajax_url' : data.ajaxUrl,
			'ajax_update_url' : data.ajaxUpdateUrl,
			'isAdmin': data.isAdmin ? true : false,

			'help': {
				'items': [],
				'loading': false,
				'page': 1,
				'pages': 1,
				'with_unpublished': data.isAdmin ? true : false,
			},

			'news': {
				'items': [],
				'loading': false,
				'page': 1,
				'pages': 1,
				'with_unpublished': data.isAdmin ? true : false,
			},

			'edit' : {
				id: '',
				title: '',
				content: '',
				content_src: [],
				is_priority: false,
				requires_confirm: false,
				published: true,
				publish_date: '',
				type: '',

				'saving': false,
			},
		},
		methods: {
			load: function(type){
				this[type].loading = true;
				$.get(
					this.ajax_url,
					{
						type: type,
						page: this[type].page,
						with_unpublished: this[type].with_unpublished,
					},
					this.on_load,
					'json'
				).error(this.on_load_error);
			},
			on_load: function(data){
				if(data.error){
					alert(data.error);
					return;
				}

				if('help' == data.type || 'news' == data.type){
					if( this[data.type].page == data.page ){
						this[data.type].pages = data.pages;
						this[data.type].items = data.items;
						this[data.type].loading = false;

						this.update_tooltips();
					}
				}

				guide('help.add_item');
				$(document).on('guide.help.add_item.closed', function(){
					guide('help.only_admin_see');
				})
			},
			on_load_error: function(xhr, ajaxOptions, thrownError){
				console.log("on_news_load_error", xhr, ajaxOptions, thrownError);
			},
			prev_page: function(type){
				this.open_page( this[type].page - 1, type );
				return false;
			},
			next_page: function(type){
				this.open_page( this[type].page + 1, type );
				return false;
			},
			open_page: function( page, type ){
				if( page < 1 ){
					page = 1;
				}

				if( page > this[type].pages ){
					page = this[type].pages;
				}

				if( this[type].page == page ){
					return;
				}

				this[type].page = page;

				this.load(type);
			},
			edit_help_new: function(type){
				if('help' != type && 'news' != type){
					return;
				}

				var now = new Date();
				var help_item = {
					id: 'new',
					title: '',
					content: '',
					content_src: [],
					is_priority: false,
					requires_confirm: false,
					published: true,
					publish_date: now.yyyymmdd(),
					type: type,
				};
				this.edit_help(help_item);
			},
			edit_help: function( news_item ){
				news_item = JSON.parse(JSON.stringify(news_item));

				if(typeof news_item.content_src == 'string'){
					var arr = JSON.parse(news_item.content_src);
					if(Array.isArray(arr)){
						news_item.content_src = arr;
					}
				}

				if(!Array.isArray(news_item.content_src)){
					news_item.content_src = [];
				}

				var date = new Date(news_item.publish_date);

				this.edit.id = news_item.id;
				this.edit.title = news_item.title;
				// this.edit.content = news_item.content;
				this.edit.content_src = news_item.content_src;
				this.edit.is_priority = news_item.is_priority;
				this.edit.requires_confirm = news_item.requires_confirm;
				this.edit.published = news_item.published;
				this.edit.publish_date = date.yyyymmdd();
				this.edit.type = news_item.type;

				console.log(news_item.content_src);

				$('#edit_help_modal').modal('open');
				$('#edit_help_modal .modal-content').scrollTop(0);

				this.update_fields();
			},
			edit_sort: function(section_idx, delta){
				if(this.edit.content_src.length <= 1){
					return;
				}

				var new_idx = section_idx + delta;
				if(new_idx < 0)
					new_idx = 0;
				if(new_idx > this.edit.content_src.length -1)
					new_idx = this.edit.content_src.length -1;

				var move_section = this.edit.content_src[section_idx];
				var content_src = [];

				for (var i = 0; i < this.edit.content_src.length; i++) {
					var section = this.edit.content_src[i];
					if(i == section_idx){
						continue;
					}

					if(content_src.length == new_idx){
						content_src.push(move_section);
					}

					content_src.push(section);

					if(content_src.length == new_idx){
						content_src.push(move_section);
					}
				}
				this.edit.content_src = content_src;
				this.update_fields();
			},
			edit_remove: function(section_idx){
				var content_src = [];
				for (var i = 0; i < this.edit.content_src.length; i++) {
					var section = this.edit.content_src[i];
					if(i == section_idx){
						continue;
					}
					content_src.push(section);
				}
				this.edit.content_src = content_src;
			},
			edit_add: function(type){
				this.edit.content_src.push({type: type, content:''});
			},
			edit_image_selected: function(event, section_idx){
				var files = event.target.files || event.dataTransfer.files;
				if (!files.length)
					return;
				this.edit_create_image(files[0], section_idx);
			},
			edit_create_image: function(file, section_idx){
				var image = new Image();
				var reader = new FileReader();
				var self = this;
				reader.onload = function(e){
					self.edit.content_src[section_idx].content = e.target.result;
				};
				reader.readAsDataURL(file);
			},
			edit_save: function(){
				if($('#edit_help_modal .modal-content input.invalid').length > 0){
					alert("Polja z rdečo so narobe izpolnjena. Prosimo popravite z rdečo označena polja.");
					return;
				}

				var news = {
					id: this.edit.id,
					title: this.edit.title,
					content: $("#edit_help_preview").html(),
					content_src: JSON.stringify(this.edit.content_src),
					is_priority: this.edit.is_priority,
					requires_confirm: this.edit.requires_confirm,
					published: this.edit.published,
					publish_date: this.edit.publish_date,
					type: this.edit.type,
				};

				this.edit.saving = true;

				$.post(
					this.ajax_update_url,
					{
						_token: token(),
						news: news,
					},
					this.on_save_help_item,
					'json'
				).error(this.on_save_help_item_error);
				
			},
			on_save_help_item: function(data){
				this.edit.saving = false;

				if( data.error ){
					alert(data.error);
					return;
				}

				if(data.type){
					this.load(data.type);
				}

				$('#edit_help_modal').modal('close');
			},
			on_save_help_item_error: function(xhr, ajaxOptions, thrownError){
				alert("Pri posodabljanju naslovnika je prišlo do napake. Prosimo poskusite kasneje.");
				this.edit.saving = false;
			},
			edit_cancel: function(){
				$('#edit_help_modal').modal('close');
			},

			delete_item: function(item){
				this[item.type].loading = true;
				$.post(
					this.ajax_update_url,
					{
						_token: token(),
						news: item,
						delete: true,
					},
					this.on_save_help_item,
					'json'
				).error(this.on_save_help_item_error);
			},

			youtubeEmberUrl: function( url ){
				var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
				var match = url.match(regExp);
				var videoId = (match&&match[7].length==11)? match[7] : false;
				return '//www.youtube.com/embed/' + videoId + '?rel=0';
			},
			update_fields: function(){
				setTimeout(function(){
					if(Materialize.updateTextFields){
						Materialize.updateTextFields();
					}
				}, 60);
			},
			update_tooltips: function(){
				setTimeout(function(){
					tooltip_init();
				}, 100);
			},

			is_draft_calc: function(date_str){
				var date = new Date(date_str);
				var curr = new Date();
				return (date < curr) ? false : true;
			},
		},
		watch: {
			'edit.publish_date': function(){
				console.log(this.edit.publish_date);
			},
			'news.with_unpublished': function(){
				this.load('news');
			},
			'help.with_unpublished': function(){
				this.load('help');
			},
		},
		mounted: function(){
			var self = this;
			
			this.load('help');
			this.load('news');

			setTimeout(function(){
				// self.edit_help_new('news');
			}, 200);

			$(he_selector).show();
		},
	});
}

/*************************************************
		HELP - END
*************************************************/