/*************************************************
		LATEST NEWS - START
*************************************************/

function ln_init(data){
	window.LNVue = new Vue({
		el: '#latest-news',
		data: {
			'ajax_url' : data.ajaxUrl,
			'profile_url' : data.profileUrl,

			'types': ['news_confirm', 'news'],
			'type': '',
			'loading': false,

			'modal_opened': false,

			'items': [],
			'confirm': false,
			'read_id': 0,
		},
		methods: {
			load_next: function(){
				if(-1 == this.types.indexOf(this.type)){
					this.type = this.types[0];
				} else if(this.types.indexOf(this.type) +1 < this.types.length) {
					this.type = this.types[this.types.indexOf(this.type) +1];
				} else {
					if(this.modal_opened){
						$('#latest-news').modal('close');
					}
				}
			},
			load: function(){
				this.loading = true;
				this.confirm = ('news_confirm' == this.type) ? true : false;
				var type = ('news_confirm' == this.type || 'news' == this.type) ? 'news' : 'help';

				$.get(
					this.ajax_url,
					{
						type: type,
						page: 1,
						unread: 1,
						confirm: this.confirm,
					},
					this.on_load,
					'json'
				).error(this.on_load_error);
			},
			on_load: function(data){
				if(data.items && data.items.length > 0){
					this.items = data.items;
					this.read_id = this.items[0].id;
					this.open_modal();
				} else {
					this.load_next();
				}
			},
			on_load_error: function(){

			},
			mark_read: function(){
				var type = ('news_confirm' == this.type || 'news' == this.type) ? 'news' : 'help';

				$.post(
					this.profile_url,
					{
						_token: token(),
						action: 'news_read',
						type: type,
						confirm: this.confirm ? 1 : 0,
						read_id: this.read_id,
					},
					this.mark_read_load,
					'json'
				).error(this.on_load_error);
			},
			mark_read_load: function(data){

			},
			open_modal: function(){
				this.loading = false;
				this.modal_opened = true;
				$("#latest-news").modal('open');
			},
			close_modal: function(){
				this.mark_read();
				this.load_next();
			}
		},
		watch: {
			'type': function(){
				this.load();
			},
		},
		mounted: function(){
			this.load_next();
		},
	});
}

/*************************************************
		LATEST NEWS - END
*************************************************/