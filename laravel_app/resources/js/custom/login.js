/*************************************************
		LOGIN - START
*************************************************/

function li_init(){
	window.LiVue = new Vue({
		el: '#login-vue',
		data: {
			username: '',
			password: '',
		},
		methods: {
			update_fields: function(){
				if(Materialize.updateTextFields){
					setTimeout(Materialize.updateTextFields, 100);
				}
			},
		},
		watch: {
			'username': function(){
				this.update_fields();
			},
		},
	});
}

/*************************************************
		LOGIN - END
*************************************************/