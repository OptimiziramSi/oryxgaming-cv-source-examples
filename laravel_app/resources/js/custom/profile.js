/*************************************************
		Profile - START
*************************************************/

function pr_init( edit_url, email ){
	window.PrVue = new Vue({
		el: '#profile-vue',
		data: {
			'edit_url' : edit_url,

			'email': email,
			'email_updating': false,
			'email_updated': false,

			'old_password': '',
			'new_password': '',
			'new_password2': '',
			'password_updating': false,
			'password_updated': false,
			'password_required': false,
		},
		methods: {
			update_email: function(){
				this.email_updating = true;
				this.email_updated = false;
				$.post(
					this.edit_url,
					{
						action: 'update_email',
						email: this.email,
						_token: token(),
					},
					this.on_update_email,
					'json'
				);
				// TODO - error handling listener
			},
			on_update_email: function(data){
				this.email_updating = false;
				if(data.error){
					alert(data.error);
					return;
				}
				this.email_updated = true;
			},
			update_password: function(){
				var errors = [];
				if('' == this.old_password){
					errors.push('Polje za staro geslo je obvezno.');
				}

				if('' == this.new_password){
					errors.push('Polje za novo geslo je obvezno.');
				}

				if('' == this.new_password2){
					errors.push('Polje za ponovni vpis novega gesla je obvezno.');
				}

				if('' != this.new_password && '' != this.new_password2 && this.new_password !== this.new_password2 ){
					errors.push('Novi gesli se ne ujemata. Prosimo ponovno vpišite novo geslo in ga ponovite v naslednjem polju.');
				}

				if(errors.length > 0){
					alert("Preden oddate zahtevo za novo geslo uredite:\n\n" + errors.join("\n\n"));
					return;
				}

				this.password_updating = true;
				this.password_updated = false;
				$.post(
					this.edit_url,
					{
						action: 'update_password',
						old_password: this.old_password,
						new_password: this.new_password,
						new_password2: this.new_password2,
						_token: token(),
					},
					this.on_update_password,
					'json'
				);
				// TODO - error handling listener
			},
			on_update_password: function(data){
				this.password_updating = false;
				if(data.error){
					alert(data.error);
					return;
				}

				this.password_updated = true;
				this.old_password = '';
				this.new_password = '';
				this.new_password2 = '';
			},
			check_password_required: function(){
				console.log("CPR");
				if( '' == this.old_password && '' == this.new_password && '' == this.new_password2 ){
					this.password_required = false;
				} else {
					this.password_required = true;
				}
			},
		},
		watch: {
			old_password: function(){
				this.check_password_required();
			},
			new_password: function(){
				this.check_password_required();
			},
			new_password2: function(){
				this.check_password_required();
			},
		},
		mounted: function(){
			
		},
	});
}

/*************************************************
		Profile - END
*************************************************/