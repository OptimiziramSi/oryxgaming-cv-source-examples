function vue_init(){
	Vue.filter('humanDate', function (value) {
		var d = new Date(value);
		return d.getDate() + "." + (d.getMonth() +1) + "." + d.getFullYear() + " " + d.getHours() + ":" + d.getMinutes();
	});

	Vue.filter('humanDate2', function (value) {
		var d = new Date(value);
		return d.getDate() + "." + (d.getMonth() +1) + "." + d.getFullYear();
	});

	Vue.filter('humanPrice', function (value) {
		if(value){
			var val = parseFloat(value);
			if(val > 0){
				return val.toFixed(2).split(".").join(",") + "€";
			} else {
				return value;
			}
		}
		return "";
	});

	Vue.filter('humanDays', function (value) {
		if(value){
			return value + " dni";
		}
		return "";
	});

	Vue.filter('humanUnit', function (count) {
		if(count){
			var len = count;
			var postfix = "kosov";
			if(1 == len)
				postfix = "kos";
			if(2 == len)
				postfix = "kosa";
			if(3 == len || 4 == len)
				postfix = "kosi";
			return len + " " + postfix;
		}
		return "";
	});
}