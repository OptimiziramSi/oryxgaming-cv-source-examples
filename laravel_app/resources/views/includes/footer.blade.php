@if(isset($no_footer_content) && true == $no_footer_content)
{{-- nothing to do here --}}
@else

@if (!Auth::guest())
<div id="latest-news" class="modal modal-fixed-footer" data-ajax-url="{{ route('help_ajax') }}" data-profile-url="{{ route('profile') }}">
	<div class="modal-content">
		<div class="row">
			<div class="col s12">
				<h2>Za vas imamo nove informacije</h2>
			</div>
		</div>
		<div class="row">
			<div class="col s12" v-for="item in items">
				<div class="card-panel grey lighten-5 z-depth-1 delivery-card-panel">
					<div v-html="item.content"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="modal-footer">
		<a href="#cancel" class="modal-action waves-effect waves-green btn red" @click.stop.prevent="close_modal" v-if="!confirm">Zapri</a>
		<a href="#cancel" class="modal-action waves-effect waves-green btn red" @click.stop.prevent="close_modal" v-if="confirm">Sprejemam</a>
	</div>
</div>
@endif

<div class="container" style="padding-top: 20px;">
	<div class="row">
		<div class="col s12 l4">
			<p class="grey-text text-lighten-4">Če v sklopu <a href="{{ route('help') }}" style="display: inline-block; text-decoration: underline; color: inherit;">pomoč in novice</a> ne najdete želenih informacij, se prosimo obrnite na Global Express.</p>
		</div>
		<div class="col s12 l4">
			<p class="grey-text text-lighten-4">
				<strong>Splošne informacije</strong><br>
				<a href="mailto:office@global-express.si" style="display: inline-block; text-decoration: underline; color: inherit;"><i class="material-icons left">email</i>office@global-express.si</a><br>
				<a href="tel:+38623301310" style="display: inline-block; text-decoration: underline; color: inherit;"><i class="material-icons left">phone</i>02 330 13 10</a>
			</p>
		</div>
		<div class="col s12 l4">
			<p class="grey-text text-lighten-4">
				<strong>Tehnična pomoč</strong><br>
				<a href="mailto:miha.puzelj@global-express.si" style="display: inline-block; text-decoration: underline; color: inherit;"><i class="material-icons left">email</i>miha.puzelj@global-express.si</a><br>
				<span><i class="material-icons left">phone</i>051 33 29 13 [ Miha Puželj ]</span>
			</p>
		</div>
	</div>
</div>
@endif
<div class="footer-copyright red" style="border-top: 1px solid #D32F2F ;">
	<div class="container">
		© {{date('Y')}} Global Express E-Tovorni List <small>(v{{env('APP_VERSION')}})</small>
		<a class="grey-text text-lighten-4 right" href="{{ route('help') }}">Preberi več</a>
	</div>
</div>