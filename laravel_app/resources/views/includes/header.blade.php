<div class="nav-wrapper">
	<a href="./" class="brand-logo"><img src="{{ asset('images/logo2.png') }}"></a>
	<a href="./" class="brand-logo center hide-on-med-and-up"><img src="{{ asset('images/logo2.png') }}"></a>
	@if(isset($no_header_menu) && true == $no_header_menu)
	{{-- nothing to do here --}}
	@else
	@if (!Auth::guest())
	<a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
	<ul id="nav-mobile" class="right hide-on-med-and-down">
		<li {!! Request::is('/') ? ' class="active"' : null !!}><a href="{{ route('delivery_new') }}"><i class="material-icons left">add_box</i>Nova pošiljka</a></li>
		<li {!! Request::is('deliveries') ? ' class="active"' : null !!}><a href="{{ route('deliveries') }}" data-guide="delivery.in_history" data-guide-position="bottom"><i class="material-icons left">history</i>Zgodovina</a></li>
		<li {!! Request::is('addresses') ? ' class="active"' : null !!}><a href="{{ route('addresses') }}"><i class="material-icons left">import_contacts</i>Naslovi</a></li>
		<li {!! Request::is('help') ? ' class="active"' : null !!}><a href="{{ route('help') }}"><i class="material-icons left">live_help</i>Pomoč in novice</a></li>
		<li {!! Request::is('profile') ? ' class="active"' : null !!}><a href="{{ route('profile') }}"><i class="material-icons left">person</i>{{ App\Partner::current()->display_name }}</a></li>
		<li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="material-icons left">exit_to_app</i>Odjava</a></li>
	</ul>
	<ul class="side-nav" id="mobile-menu">
		<li {!! Request::is('/') ? ' class="active"' : null !!}><a href="{{ route('delivery_new') }}"><i class="material-icons left">add_box</i>Nova pošiljka</a></li>
		<li {!! Request::is('deliveries') ? ' class="active"' : null !!}><a href="{{ route('deliveries') }}"><i class="material-icons left">history</i>Zgodovina</a></li>
		<li {!! Request::is('addresses') ? ' class="active"' : null !!}><a href="{{ route('addresses') }}"><i class="material-icons left">import_contacts</i>Naslovi</a></li>
		<li {!! Request::is('help') ? ' class="active"' : null !!}><a href="{{ route('help') }}"><i class="material-icons left">live_help</i>Pomoč in novice</a></li>
		<li {!! Request::is('profile') ? ' class="active"' : null !!}><a href="{{ route('profile') }}"><i class="material-icons left">person</i>{{ App\Partner::current()->display_name }}</a></li>
		<li><a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="material-icons left">exit_to_app</i>Odjava</a></li>
	</ul>
	<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
		{{ csrf_field() }}
	</form>
	@elseif(!Request::is('login'))
	<a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
	<ul id="nav-mobile" class="right hide-on-med-and-down">
		<li><a href="{{ route('login') }}">Vpis v Global Express</a></li>
	</ul>
	<ul class="side-nav" id="mobile-menu">
		<li><a href="{{ route('login') }}">Vpis v Global Express</a></li>
	</ul>
	@endif
	@endif
</div>