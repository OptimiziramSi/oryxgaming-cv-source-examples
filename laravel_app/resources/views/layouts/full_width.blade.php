@extends('layouts.wrapper')
@section('main')
<div class="row">
	<div class="col s12">
		@yield('content')
	</div>
</div>
@stop