@extends('layouts.full_width')
@section('content')
<div class="row">
	<div class="col s12 m6 offset-m3 l4 offset-l4">
		<h1 class="center-align">404</h1>
		<h3 class="center-align">Ne najdem izbrane strani</h3>
		<p class="center-align">
			<a href="{{ route('delivery_new') }}" class="btn red">Pojdi Domov</a>
		</p>
	</div>
</div>
@stop