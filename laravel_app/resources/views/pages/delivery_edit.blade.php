@extends('layouts.full_width')
@section('content')
<div id="delivery-edit-vue" class="row" data-ajax-url="{{ $ajax_json }}" data-addresses-url="{{ $addresses_json }}" data-ajax-post-url="{{ $ajax_post }}" data-delivery-id="{{ $delivery_id }}" data-package-types="{{ json_encode( $package_types ) }}" style="display:none;">
	<ul class="collapsible" data-collapsible="accordion" data-duration="550" v-show="!loading && true === edit_enabled">
		<li>
			<div class="collapsible-header"><i class="material-icons">place</i>Korak 1 od 3 - Podatki o naslovniku in pošiljki</div>
			<div class="collapsible-body section">
				<div class="row">
					<div class="col s12">
						<a class="btn red right" @click="save_delivery" :class="{ disabled: delivery_saving }">
							<i class="material-icons right">keyboard_arrow_right</i>Izberite storitev
						</a>
						<div class="preloader-wrapper small active right" style="margin-right: 10px;" v-show="delivery_saving">
							<div class="spinner-layer spinner-red-only">
								<div class="circle-clipper left"><div class="circle"></div></div>
								<div class="gap-patch"><div class="circle"></div></div>
								<div class="circle-clipper right"><div class="circle"></div></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row" v-show="!loading">
					<div class="col s12 m12 l4">
						<h2>Naslovnik</h2>
						<div class="row">
							<div class="col s12 z-depth-2 hoverable" data-guide="delivery.search_addressee" data-guide-position="top" style="padding-top: 1rem; padding-bottom: 0.5rem; margin-bottom: 0.5rem;">
								<h4 style="padding-bottom: 1rem;">Ste že pošiljali izbranemu naslovniku? Zakaj ne uporabite iskalnika?</h4>
								<section class="input-field">
									<input id="edit_delivery_search_address" :disabled="delivery_saving" type="search" v-model="search" @keyup.up="select_search_item(-1)" @keyup.down="select_search_item(1)" @keyup.enter="search_result_active ? set_address(search_result_active) : undefined;">
									<label for="edit_delivery_search_address"><i class="material-icons">search</i> išči med naslovniki</label>
									<i class="material-icons" @click="reset_search">close</i>
								</section>
								<section v-show="search">
									<div class="progress" v-show="search_loading">
										<div class="indeterminate"></div>
									</div>
									<div class="card-panel grey lighten-5 z-depth-1 address-card-panel" v-show="!search_loading && !search_has_results">
										Ni rezultatov za iskanje "@{{ search }}"
									</div>
									<div class="card-panel address-card-panel" v-show="!search_loading && search_has_results" @click.stop.prevent="set_address(address)" v-for="address in search_result" style="cursor: pointer;" :class="{ 'grey lighten-5 z-depth-1': search_result_active != address, 'z-depth-5 green lighten-2': search_result_active == address }" @mouseover="search_result_active = address;" @mouseout="search_result_active = undefined;">
										<div class="row valign-wrapper">
											<div class="col s4">
												<span class="black-text">
													@{{ address.addressee_1 }} <br v-show="address.addressee_2">
													@{{ address.addressee_2 }}
												</span>
											</div>
											<div class="col s4">
												<span class="black-text">
													@{{ address.street_1 }} <br v-show="address.naslovUlica_2 && address.street_2">
													@{{ address.street_2 }} <br v-show="address.naslovPosta || address.city">
													@{{ address.post_number }} @{{ address.city }} <br v-show="address.country_display_name">
													@{{ address.country_display_name }}
												</span>
											</div>
											<div class="col s4">
												<span class="black-text">
													<span class="grey-text hide-on-large-only">Kontakt:<br></span>
													<i class="tiny material-icons" v-show="address.naslovKontaktIme">perm_identity</i> @{{ address.contact_name }}<br v-show="address.contact_phone">
													<a :href="'tel:' + address.contact_phone" v-show="address.contact_phone"><i class="tiny material-icons">call</i> @{{ address.contact_phone }}</a><br v-show="address.contact_email">
													<a :href="'mailto:' + address.contact_email" v-show="address.contact_email"><i class="tiny material-icons">email</i> @{{ address.contact_email }}</a>
												</span>
											</div>
										</div>
									</div>
								</section>
							</div>
							<div v-show="search == ''">
								<div class="input-field col s12">
									<input v-model="delivery.addressee_1" id="delivery_addressee_1" type="text" class="validate required" :disabled="delivery_saving">
									<label for="delivery_addressee_1">Uradni naziv naslovnika</label>
								</div>
								<div class="input-field col s12">
									<input v-model="delivery.addressee_2" id="delivery_addressee_2" type="text" class="validate" :disabled="delivery_saving" >
									<label for="delivery_addressee_2">(neobvezno) podrobnosti o naslovniku</label>
								</div>
								<div class="input-field col s12">
									<input v-model="delivery.street_1" id="delivery_street_1" type="text" class="validate required" :disabled="delivery_saving" >
									<label for="delivery_street_1">Ulica</label>
								</div>
								<div class="input-field col s12">
									<input v-model="delivery.street_2" id="delivery_street_2" type="text" class="validate" :disabled="delivery_saving" >
									<label for="delivery_street_2">(neobvezno) Poslovna enota / nadstropje</label>
								</div>
								<div class="input-field col s4 m4 l2">
									<input v-model="delivery.post_number" id="delivery_post_number" type="text" class="validate required" :pattern="post_number_pattern ? post_number_pattern : undefined" :disabled="delivery_saving">
									<label for="delivery_post_number">Poštna št.</label>
								</div>
								<div class="input-field col s8 m8 l4">
									<input v-model="delivery.city" id="delivery_city" type="text" class="validate required" :disabled="delivery_saving" >
									<label for="delivery_city">Kraj</label>
								</div>
								<div class="input-field col s12 m12 l6 country-field">
									<select id="edit_delivery_country" onchange="DeeVue.$emit('edit_delivery_country_changed', $(this).val(), $(this).find('option:selected').text(), $(this).find('option:selected').attr('data-post-number-pattern'));" class="required" :disabled="delivery_saving">
										<option disabled selected>Izberite državo</option>
										@foreach($countries as $country)
										<option value="{{ $country->id }}" data-post-number-pattern="{{ $country->post_number_pattern }}">{{ $country->display_name }}</option>
										@endforeach
									</select>
									<label>Država</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col s12 m6 l4">
						<h2>Kontaktni podatki</h2>
						<div class="row">
							<div class="input-field col s12">
								<input v-model="delivery.contact_name" id="delivery_contact_name" type="text" class="validate" :disabled="delivery_saving">
								<label for="delivery_contact_name">Ime</label>
							</div>
							<div class="input-field col s12 m6 l6">
								<input v-model="delivery.contact_phone" id="delivery_contact_phone" type="text" class="validate required" :disabled="delivery_saving">
								<label for="delivery_contact_phone">Telefon</label>
							</div>
						</div>
						<h2>Opombe k pošiljki</h2>
						<div class="row">
							<div class="input-field col s12">
								<textarea id="delivery_comment" class="materialize-textarea validate" v-model="delivery.comment" :disabled="delivery_saving"></textarea>
								<label for="delivery_comment">(neobvezno) Vnesite opombe k pošiljki, če so te potrebne</label>
							</div>
							
						</div>
					</div>
					<div class="col s12 m6 l4">
						<h2>Tip in dimenzije pošiljke</h2>
						<div class="row">
							<div class="col s12 package-type-choices no-padding">
								<h4>Izberite tip pošiljke</h4>
								<div class="row no-margin">
									<div class="col s12 m4 l4" v-for="package_type, pt_idx in package_types" v-if="package_type.active">
										<p>
											<input :id="'pt_' + pt_idx" name="delivery.type_id" class="with-gap" :class="{ required: !delivery.type_id }" type="radio" :value="package_type.id" v-model="delivery.type_id" :disabled="delivery_saving">
											<label :for="'pt_' + pt_idx">@{{ package_type.display_name }}</label>
										</p>
									</div>
								</div>
								<div class="row"></div>
							</div>
							<div class="col s12 package-type-choices" v-show="package_has_dimension || package_has_weight">
								<h4>Vnesite dimenzije pošiljke <span class="right">[ Št. pošiljk: <strong><span style="font-size: 1.3rem;">@{{ packages_count }}</span></strong> ]</span></h4>
								<div class="row" style="margin-bottom: 0px;" v-for="(package, p_idx) in delivery.packages">
									<div class="input-field col s3" v-if="package_has_weight">
										<input :id="'teza_' + p_idx" type="text" class="validate right-align" v-bind:class="{ required: package.required }" :pattern="number_pattern" v-model="package.weight" @change="check_packages" :disabled="delivery_saving">
										<label :for="'teza_' + p_idx">Teža [kg]</label>
									</div>
									<div class="input-field col s3" v-if="package_has_dimension">
										<input :id="'dolzina_' + p_idx" type="text" class="validate right-align" :class="{ required: package.required }" :pattern="number_pattern" v-model="package.depth" @change="check_packages" :disabled="delivery_saving">
										<label :for="'dolzina_' + p_idx">Dolžina [cm]</label>
									</div>
									<div class="input-field col s3" v-if="package_has_dimension">
										<input :id="'sirina_' + p_idx" type="text" class="validate right-align" :class="{ required: package.required }" :pattern="number_pattern" v-model="package.width" @change="check_packages" :disabled="delivery_saving">
										<label :for="'sirina_' + p_idx">Širina [cm]</label>
									</div>
									<div class="input-field col s3" v-if="package_has_dimension">
										<input :id="'visina_' + p_idx" type="text" class="validate right-align" :class="{ required: package.required }" :pattern="number_pattern" v-model="package.height" @change="check_packages" :disabled="delivery_saving">
										<label :for="'visina_' + p_idx">Višina [cm]</label>
									</div>
								</div>
							</div>
							<div class="col s12 package-type-choices" v-show="delivery.type_id && !package_has_dimension && !package_has_weight">
								<h2>Število dokumentov</h2>
								<div class="row" style="margin-bottom: 0px;">
									<div class="input-field col s6">
										<input id="delivery_packages_count" type="text" class="validate right-align" :class="{ required: !package_has_dimension && !package_has_weight }" :pattern="number_pattern" v-model="delivery.packages_count" :disabled="delivery_saving">
										<label for="delivery_packages_count">Vnesite število dokumentov</label>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<a class="btn red right" @click="save_delivery" :class="{ disabled: delivery_saving }">
							<i class="material-icons right">keyboard_arrow_right</i>Izberite storitev
						</a>
						<div class="preloader-wrapper small active right" style="margin-right: 10px;" v-show="delivery_saving">
							<div class="spinner-layer spinner-red-only">
								<div class="circle-clipper left"><div class="circle"></div></div>
								<div class="gap-patch"><div class="circle"></div></div>
								<div class="circle-clipper right"><div class="circle"></div></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li>
			<div class="collapsible-header"><i class="material-icons">forward</i>Korak 2 od 3 - Izberi storitev</div>
			<div class="collapsible-body section">
				<div class="row">
					<div class="col s12">
						<a class="btn red left" @click="prev_tab" :class="{ disabled: service_saving }"><i class="material-icons left">keyboard_arrow_left</i><span class="hide-on-small-only">Nazaj</span></a>
						<a class="btn red right" @click="save_service" :class="{ disabled: service_saving }"><i class="material-icons right">keyboard_arrow_right</i>Oddajte naročilo</a>
						<div class="preloader-wrapper small active right" style="margin-right: 10px;" v-show="service_saving">
							<div class="spinner-layer spinner-red-only">
								<div class="circle-clipper left"><div class="circle"></div></div>
								<div class="gap-patch"><div class="circle"></div></div>
								<div class="circle-clipper right"><div class="circle"></div></div>
							</div>
						</div>
					</div>
				</div>
				<div class="row" v-show="!loading">
					<div class="col s12 m4 l2" :class="{ clickable: service.available }" v-for="service in services" @click="set_service(service)" v-show="service.available || service.comment">
						<div class="card small z-depth-2" :class="{ hoverable: service.available, 'z-depth-5': selected_service == service }">
							<div class="card-content no-padding center-align">
								<img :src="'data:image/png;base64,' + service.image" style="height: 100px; width: auto;">
								<h3 class="center-align">@{{ service.display_name }}</h3>
								<h4 class="center-align">@{{ service.code }}</h4>
								<h3 class="center-align">@{{ service.price | humanPrice }}</h3>
								<h4 class="center-align">@{{ service.eta | humanDays }}</h4>
								<h5 class="center-align">@{{ service.price_comment }}</h5>
								<h5 class="center-align">@{{ service.comment }}</h5>
							</div>
							<div class="card-action center-align">
								<a class="btn red" :class="{ disabled: !service.available }" v-show="service != selected_service" :class="{ disabled: service_saving }">Izberi</a>
								<a class="btn green" :class="{ disabled: !service.available || service_saving }" v-show="service == selected_service" :class="{ disabled: service_saving }">Izbrano</a>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col s12">
						<a class="btn red left" @click="prev_tab" :class="{ disabled: service_saving }"><i class="material-icons left">keyboard_arrow_left</i><span class="hide-on-small-only">Nazaj</span></a>
						<a class="btn red right" @click="save_service" :class="{ disabled: service_saving }"><i class="material-icons right">keyboard_arrow_right</i>Oddajte naročilo</a>
						<div class="preloader-wrapper small active right" style="margin-right: 10px;" v-show="service_saving">
							<div class="spinner-layer spinner-red-only">
								<div class="circle-clipper left"><div class="circle"></div></div>
								<div class="gap-patch"><div class="circle"></div></div>
								<div class="circle-clipper right"><div class="circle"></div></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</li>
		<li>
			<div class="collapsible-header"><i class="material-icons">print</i>Korak 3 od 3 - Natisni tovorni list</div>
			<div class="collapsible-body section">
				<div class="row">
					<div class="col s12 l8">
						<h4>Povezave za slednje paketu. Povezavo lahko posredujete stranki, ki pričakuje paket.</h4>
						<ul>
							<li v-for="tracking_url in tracking_urls">V jeziku @{{ tracking_url.language }}: <strong>@{{ tracking_url.url }}</strong> <a :href="tracking_url.url" target="_blank">prikaži</a></li>
						</ul>
					</div>
					<div class="col s12 l3">
						<a class="btn red right" :href="pdf_url_download" data-guide="delivery.can_download_pdf" data-guide-position="bottom"><i class="material-icons left">file_download</i>Prenesite PDF</a>
					</div>
				</div>
				<div class="row">
					<div class="col s12" v-show="pdf_loading">
						<div class="progress">
							<div class="indeterminate"></div>
						</div>
					</div>
					<iframe class="col s12" style="min-height: 600px; padding: 0px;" :src="pdf_viewer_url" v-on:load="pdf_loading = false;"></iframe>
				</div>
				<div class="row">
					<div class="col s12">
						<a class="btn red right" :href="pdf_url_download"><i class="material-icons left">file_download</i>Prenesite PDF</a>
					</div>
				</div>
			</div>
		</li>
	</ul>
	<div class="col s12" v-show="!loading && false === edit_enabled">
		<div class="card-panel grey lighten-5 z-depth-3 delivery-card-panel">
			<p>Izbrane pošiljke ni mogoče urejati, ker je že oddana. Pošiljko si lahko ogledati na seznamu v <a href="{{ route('deliveries') }}" class="btn red hoverable"><i class="material-icons left">history</i>Zgodovina</a>.</p>
		</div>
	</div>
	<div class="col s12" v-show="loading && 0 == tab">
		<div class="progress" v-show="loading">
			<div class="indeterminate"></div>
		</div>
	</div>
</div>
@stop