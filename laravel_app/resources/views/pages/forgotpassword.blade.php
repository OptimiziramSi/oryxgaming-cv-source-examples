@extends('layouts.full_width')
@section('content')
<div class="row">
	@if (session('success'))
	<div class="col s12 m6 offset-m3 l4 offset-l4">
		<div class="card-panel green white-text z-depth-5">
			<p>Poslali smo vam sporočilo s povezavo za ponastavitev gesla.</p>
			<p>Prosimo preverite vaš poštni predal.<br>Opomba: poglejte tudi v mapo za nezaželeno pošto.</p>
			<p>V redkih primerih lahko traja tudi nekaj minut, da prejmete sporočilo.</p>
		</div>
	</div>
	@else
	
	<form class="col s12 m6 offset-m3 l4 offset-l4" action="{{ route('forgotpassword') }}" method="post">
		{{ csrf_field() }}
		@if (session('error'))
		<div class="row card-panel red lighten-1 z-depth-5 white-text">
			<div class="col s12">
				<h3>Opozorilo</h3>
				<p>{{ session('error') }}</p>
			</div>
		</div>
		@endif
		<div class="row card-panel grey lighten-5 z-depth-1">
			<div class="col s12">
				<h3>{{ trans('Pozabljeno geslo') }}</h3>
				<p>Če ste v svojem uporabniškem profilu nastavili email, lahko geslo resetirate kar sami. V spodnje polje vnesite vaše uporabniško ime za vpis v Prizmo in kliknite "POŠLJI POVEZAVO ZA PONASTAVITEV GESLA".</p>
				<p>V primeru, da se ne spomnite vašega uporabniškega imena ali emaila niste nastavili, se prosimo obrnite na Global Express na enega izmed spodnjih kontaktov.</p>
				<p>
					<a href="tel:+38651332913" class="btn red"><i class="material-icons left">phone</i>041 877 002 [ Miha Puželj ]</a><br><br>
					<a href="mailto:miha.puzelj@global-express.si" class="btn small red"><i class="material-icons left">email</i>miha.puzelj@global-express.si</a>
				</p>
			</div>
		</div>
		<div class="row card-panel grey lighten-5 z-depth-1">
			<div class="col s12">
				<h3>Ponastavitev gesla</h3>
			</div>
			<div class="input-field col s12">
				<input id="username" type="text" name="username" class="validate required" value="{{ old('username') }}" placeholder="Vpišite uporabniško ime">
				<label for="username">{{ trans('Uporabniško ime')}}</label>
			</div>
			<div class="col s12">
				<button class="btn waves-effect waves-light" type="submit" name="action">{{ trans('Pošlji povezavo za ponastavitev gesla') }}<i class="material-icons right">send</i></button>
			</div>
		</div>
	</form>
	@endif
</div>
@stop