@extends('layouts.full_width')
@section('content')
<div class="row">
	@if( ! $delivery )
		<div class="card-panel grey lighten-5 z-depth-1 col s12 m10 offset-m1 l8 offset-l2">
		<div class="row">
			<div class="col s12">
				<h3>{{ trans('tracking.delivery_not_found') }}</h3>
			</div>
		</div>
	</div>
	@else
	<div class="card-panel grey lighten-5 z-depth-1 col s12 m10 offset-m1 l8 offset-l2">
		<div class="row">
			<div class="col s12">
				<h3>{{ trans('tracking.delivery_code') }}: {{ $delivery['code'] }}</h3>
			</div>
		</div>
	</div>
	<div class="card-panel grey lighten-5 z-depth-1 col s12 m10 offset-m1 l8 offset-l2">
		<div class="row">
			<div class="col s12">
				<p>{{ trans('tracking.courier') }}: <strong>{{ $delivery['courier_display_name'] }}</strong></p>
				<p>{{ trans('tracking.courier_awb') }}: <strong>{{ $delivery['courier_awb'] }}</strong></p>
			</div>
		</div>
	</div>
	<div class="card-panel grey lighten-5 z-depth-1 col s12 m10 offset-m1 l8 offset-l2">
		<div class="row valign-wrapper">
			<div class="col s3">
				<span class="black-text">{{ trans('tracking.column_date') }}</span>
			</div>
			<div class="col s3">
				<span class="black-text">{{ trans('tracking.column_type') }}</span>
			</div>
			<div class="col s6">
				<span class="black-text">{{ trans('tracking.column_description') }}</span>
			</div>
		</div>
	</div>
	@if( 0 == count( $delivery['tracking'] ) )
	<div class="card-panel grey lighten-5 z-depth-1 col s12 m10 offset-m1 l8 offset-l2">
		<div class="row">
			<div class="col s12">
				<p>{{ trans('tracking.no_tracking_data_please_try_later') }}</p>
			</div>
		</div>
	</div>
	@else
	@foreach( $delivery['tracking'] as $tracking )
	<div class="card-panel grey lighten-5 z-depth-1 col s12 m10 offset-m1 l8 offset-l2">
		<div class="row valign-wrapper">
			<div class="col s3">
				<span class="black-text">{{ date( trans('tracking.date_format'), strtotime( $tracking->updated ) ) }}</span>
			</div>
			<div class="col s3">
				<span class="black-text">{{ $tracking->location }}</span>
			</div>
			<div class="col s6">
				<span class="black-text">{{ $tracking->description }}</span>
			</div>
		</div>
	</div>
	@endforeach
	@endif
	@endif
</div>
@stop